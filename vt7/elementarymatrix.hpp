/**
    \file elementarymatrix.hpp
    \brief Header for ElementarySquareMatrix template class, SymbolicSquareMatrix class and ConcreteSquareMatrix class
 */
#ifndef ELEMENTARYMATRIX_H_INCLUDED
#define ELEMENTARYMATRIX_H_INCLUDED
#include "element.hpp"
#include <vector>

/**
    \brief Template class ElementarySquareMatrix
    \tparam Type IntElement or VariableElement
 */
template<typename Type> class ElementarySquareMatrix {
    private:
        int n;
        std::vector<std::vector<std::unique_ptr<Type>>> elements;
        friend ElementarySquareMatrix<Element>;     
    public:

        /**
         * @brief Construct a new Elementary Square Matrix object
         */
        ElementarySquareMatrix():n(0) {};

        /**
         * @brief Construct a new Elementary Square Matrix object
         * @param str_m std::string representing a square matrix
         */
        explicit ElementarySquareMatrix(const std::string& str_m);

        /**
         * @brief Construct a new Elementary Square Matrix object (copy)
         * @param m Matrix to be copied from
         */
        ElementarySquareMatrix(const ElementarySquareMatrix& m);

        /**
         * @brief Construct a new Elementary Square Matrix object (move copy)
         * @param m Matrix to be copied from
         */
        ElementarySquareMatrix(ElementarySquareMatrix&& m);

        /**
         * @brief Destroy the Elementary Square Matrix object
         */
        virtual ~ElementarySquareMatrix()=default;

        /**
         * @brief Copy assignment operator
         * @param m Matrix to be copied from
         * @return ElementarySquareMatrix& 
         */
        ElementarySquareMatrix& operator = (const ElementarySquareMatrix& m);

        /**
         * @brief Move copy assignment operator
         * @param m Matrix to be copied from
         * @return ElementarySquareMatrix& 
         */
        ElementarySquareMatrix& operator = (ElementarySquareMatrix&& m);

        /**
         * @brief Transpose the matrix
         * @return ElementarySquareMatrix 
         */
        ElementarySquareMatrix transpose() const;

        /**
         * @brief Operator +
         * @param m Matrix to be added
         * @return ElementarySquareMatrix 
         */
        ElementarySquareMatrix operator+(const ElementarySquareMatrix& m) const;

        /**
         * @brief Operator -
         * @param m Matrix to be subtracted
         * @return ElementarySquareMatrix 
         */
        ElementarySquareMatrix operator-(const ElementarySquareMatrix& m) const;

        /**
         * @brief Operator *
         * @param m Matrix to be multiplied
         * @return ElementarySquareMatrix 
         */
        ElementarySquareMatrix operator*(const ElementarySquareMatrix& m) const;

        /**
         * @brief Operator +=
         * @param m Matrix to be added
         * @return ElementarySquareMatrix& 
         */
        ElementarySquareMatrix& operator += (const ElementarySquareMatrix& m);

        /**
         * @brief Operator -=
         * @param m Matrix to be subtracted
         * @return ElementarySquareMatrix& 
         */
        ElementarySquareMatrix& operator -= (const ElementarySquareMatrix& m);

        /**
         * @brief Operator *=
         * @param m Matrix to be multiplied
         * @return ElementarySquareMatrix& 
         */
        ElementarySquareMatrix& operator *= (const ElementarySquareMatrix& m);

        /**
         * @brief Comparison operator
         * @param m Matrix to be compared
         * @return bool
         */
        bool operator == (const ElementarySquareMatrix& m) const;

        /**
         * @brief Print function for matrices
         * @param os std::ostream& to print to
         */
        void print(std::ostream& os) const;

        /**
         * @brief Function to transform matrix to a string
         * @return std::string 
         */
        std::string toString() const;

        /**
         * @brief Construct a ConcreteSquareMatrix from SymbolicSquareMatrix
         * @param val Valuation map containing variables
         * @return ElementarySquareMatrix<IntElement> 
         */
        ElementarySquareMatrix<IntElement> evaluate(const Valuation& val) const;

        /**
         * @brief Getter for param n
         * @return int 
         */
        int getN() const {return n;}
};

using ConcreteSquareMatrix = ElementarySquareMatrix<IntElement>;
using SymbolicSquareMatrix = ElementarySquareMatrix<Element>;

template<typename T> ElementarySquareMatrix<T>::ElementarySquareMatrix(const ElementarySquareMatrix& m) {
    for(auto& mrow : m.elements) {
        elements.push_back({}); // push back an empty row
        for(auto& melem : mrow) {
            elements.back().push_back(std::unique_ptr<T>(dynamic_cast<T*>(melem->clone()))); // push back a clone of the element
        }
    }
    n = m.n;
}

template<typename T> ElementarySquareMatrix<T>::ElementarySquareMatrix(ElementarySquareMatrix&& m) : elements(std::move(m.elements)) { // move copy constructor
    n = m.n;
}

template<typename T> ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator = (const ElementarySquareMatrix& m) {
    if (this == &m) return *this; // if trying to assign onto itself

    ElementarySquareMatrix<T> tmpMtrx{m};
    std::swap(elements, tmpMtrx.elements);
    n = m.n;
    return *this;
}

template<typename T> ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator = (ElementarySquareMatrix&& m) {
    if (this == &m) return *this; // if trying to assign onto itself

    elements = std::move(m.elements);
    n = m.n;
    return *this;
}

template<typename T> ElementarySquareMatrix<T> ElementarySquareMatrix<T>::transpose() const {
    ElementarySquareMatrix<T> tmpMtrx;
    tmpMtrx.n = n;

    for (int i=0;i<n;i++) { // row
        tmpMtrx.elements.push_back({});
        for (int j=0;j<n;j++) { // column
            tmpMtrx.elements.at(i).push_back(std::unique_ptr<T>(dynamic_cast<T*>(elements.at(j).at(i)->clone())));
        }
    }
    return tmpMtrx;
}

template<typename T> bool ElementarySquareMatrix<T>::operator == (const ElementarySquareMatrix<T>& m) const { // comparison operator
    if (n != m.n) {
        return false; // if dimensions don't match, clearly not equal
    }
    return (toString() == m.toString()); // equal matrices should transform into equal strings
}

template<typename T> void ElementarySquareMatrix<T>::print(std::ostream& os) const {
    os << toString();
}

template<typename T> std::string ElementarySquareMatrix<T>::toString() const { // return a string representation of the matrix
    std::stringstream strm;
    strm << "[";
    for (const auto& row : elements) { // row
        strm << "[";
        bool first = true;
        for (const auto& elem : row) { // element in the row
            if (!first) { // don't put a comma before the first number
                 strm << ",";
            } 
            strm << *elem;
            first = false;
        }
        strm << "]";
    }
    strm << "]";

    return strm.str();
}

template<typename T> ElementarySquareMatrix<IntElement> ElementarySquareMatrix<T>::evaluate(const Valuation& val) const {
    ConcreteSquareMatrix csm;  // Initialize an empty ConcreteSquareMatrix
    csm.n = n;

    for(auto& row : elements) {
        csm.elements.push_back({}); // Add row
        for(auto& elem : row) {
            csm.elements.back().push_back(std::unique_ptr<IntElement>(new IntElement(elem->evaluate(val)))); // Add element
        }
    }
    return csm;
}

template<typename T> std::ostream& operator<<(std::ostream& os, const ElementarySquareMatrix<T>& sm) {
    os << sm.toString();
    return os;
}
#endif