/**
    \file valuation_tests.cpp
    \brief Unit tests for Valuation class
*/
#include "catch.hpp"
#include "element.hpp"
#include "valuation.hpp"

TEST_CASE("Valuation tests, correct", "[Valuation]") {
    Valuation valu;
    valu['x'] = 5;
    VariableElement varel{'x'};
    VariableElement varel2{'z'};
    CHECK(varel.evaluate(valu) == 5);
    CHECK_THROWS(varel2.evaluate(valu));
}

TEST_CASE("Valuation tests, incorrect", "[Valuation]") {
    Valuation valu;
    valu['x'] = 5;
    VariableElement varel2{'z'};
    CHECK_THROWS(varel2.evaluate(valu));
}