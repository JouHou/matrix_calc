/**
    \file valuation.hpp
    \brief Header for Valuation class
*/
#ifndef VALUATION_H_INCLUDED
#define VALUATION_H_INCLUDED
#include <map>

using Valuation = std::map<char,int>;
#endif