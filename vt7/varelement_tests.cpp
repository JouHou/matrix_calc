/**
    \file varelement_tests.cpp
    \brief Unit tests for VariableElement class
*/
#include "catch.hpp"
#include "element.hpp"
#include "valuation.hpp"
#include <sstream>
#include <memory>

TEST_CASE("VarElement default constructor test", "[VarElement]") {
    VariableElement ie;
    CHECK(ie.getVal() == 0);
}

TEST_CASE("VarElement parametric constructor test", "[VarElement]") {
    VariableElement ie{'z'};
    CHECK(ie.getVal() == 'z');
}

TEST_CASE("VarElement setVal() test", "[VarElement]") {
    VariableElement ie;
    ie.setVal('y');
    CHECK(ie.getVal() == 'y');
}

TEST_CASE("VarElement clone() test", "[VarElement]") {
    VariableElement ie{'y'};
    VariableElement ie2 = *std::unique_ptr<VariableElement>(dynamic_cast<VariableElement*>(ie.clone()));
    CHECK(ie2.getVal() == 'y');
}

TEST_CASE("VarElement toString() test", "[VarElement]") {
    VariableElement ie{'y'};
    CHECK(ie.toString() == "y");
}

TEST_CASE("VarElement evaluate() test", "[VarElement]") {
    Valuation valu;
    valu['y'] = 222;
    VariableElement ie{'y'};
    CHECK(ie.evaluate(valu) == 222);
}

TEST_CASE("VarElement operator == test", "[VarElement]") {
    VariableElement ie{'y'};
    VariableElement ie2{'y'};
    CHECK(ie == ie2);
}