Matrix calculator

A school project for University of Oulu's Advanced Object-Oriented Programming course.

Technologies used:

- C++ (-std==c++11)
- Catch2: unit tests written with Catch2 framework
- Doxygen: comments written as doxygen-compatible
- Cppcheck: static code analysis with cppcheck
- Gcov: code coverage analysis for unit tests performed with gcov
- Valgrind: memory leak detection performed with valgrind
