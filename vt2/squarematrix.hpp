/**
    \file squarematrix.hpp
    \brief Header for SquareMatrix class
*/
#ifndef SQUAREMATRIX_H_INCLUDED
#define SQUAREMATRIX_H_INCLUDED
#include <ostream>
#include "intelement.hpp"

/**
    \class SquareMatrix
    \brief A class for 2x2 square matrices
*/
class SquareMatrix {
    private:
         IntElement e11;
         IntElement e12;
         IntElement e21;
         IntElement e22;
    public:
        /**
            \brief Default constructor
        */
        SquareMatrix();

        /**
            \brief Parametric constructor
            \param e11 Matrix element 1,1
            \param e12 Matrix element 1,2
            \param e21 Matrix element 2,1
            \param e22 Matrix element 2,2
        */        
        SquareMatrix(const IntElement& e11,const IntElement&  e12,const IntElement&  e21,const IntElement&  e22);

        /**
            \brief Copy constructor
            \param m SquareMatrix to be copied
        */
        SquareMatrix(const SquareMatrix& m);

        /**
            \brief Default destructor
        */
        virtual ~SquareMatrix()=default;

        /**
            \brief Overloaded operator +=
            \param m SquareMatrix to be added
            \return The resulting SquareMatrix
        */
        SquareMatrix& operator += (const SquareMatrix& m);

        /**
            \brief Overloaded operator -=
            \param m SquareMatrix to be subtracted
            \return The resulting SquareMatrix
        */
        SquareMatrix& operator -= (const SquareMatrix& m);

        /**
            \brief Overloaded operator *=
            \param m SquareMatrix to be multiplied
            \return The resulting SquareMatrix
        */
        SquareMatrix& operator *= (const SquareMatrix& m);

        /**
            \brief Print function for IntElement class, outputs to std::ostream
            \param os The output stream for printing
        */
        void print(std::ostream& os) const;
        
        /**
            \brief Function to transform the SquareMatrix to a string
            \return The resulting string
        */
        std::string toString() const;
};
#endif