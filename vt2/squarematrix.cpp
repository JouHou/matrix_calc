/**
    \file squarematrix.cpp
    \brief Source code for SquareMatrix class
*/
#include "squarematrix.hpp"
#include "intelement.hpp"
#include <iostream>
#include <sstream>

SquareMatrix::SquareMatrix() : SquareMatrix{0,0,0,0} {}
SquareMatrix::SquareMatrix(const IntElement& e11,const IntElement& e12,
    const IntElement& e21,const IntElement& e22) {
    this->e11 = e11;
    this->e12 = e12;
    this->e21 = e21;
    this->e22 = e22;
}
SquareMatrix::SquareMatrix(const SquareMatrix& m) {
    e11 = m.e11;
    e12 = m.e12;
    e21 = m.e21;
    e22 = m.e22;
}
SquareMatrix& SquareMatrix::operator += (const SquareMatrix& m) {
    e11 = e11 + m.e11;
    e12 = e12 + m.e12;
    e21 = e21 + m.e21;
    e22 = e22 + m.e22;
    return *this;
}
SquareMatrix& SquareMatrix::operator -= (const SquareMatrix& m) {
    e11 = e11 - m.e11;
    e12 = e12 - m.e12;
    e21 = e21 - m.e21;
    e22 = e22 - m.e22;
    return *this;
}
SquareMatrix& SquareMatrix::operator *= (const SquareMatrix& m) {
    IntElement tmp_e11, tmp_e12, tmp_e21, tmp_e22;
    tmp_e11 = e11 * m.e11 + e12 * m.e21;
    tmp_e12 = e11 * m.e12 + e12 * m.e22;
    tmp_e21 = e21 * m.e11 + e22 * m.e21;
    tmp_e22 = e21 * m.e12 + e22 * m.e22;
    e11 = tmp_e11;
    e12 = tmp_e12;
    e21 = tmp_e21;
    e22 = tmp_e22;
    return *this;
}
void SquareMatrix::print(std::ostream& os) const {
    os << toString();
}
std::string SquareMatrix::toString() const {
    std::stringstream strm;
    strm << "[[" << e11.getVal() << "," << e12.getVal() << "][";
    strm << e21.getVal() << "," << e22.getVal() << "]]";
    return strm.str();
}