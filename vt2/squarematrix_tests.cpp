/**
    \file squarematrix_tests.cpp
    \brief Tests for SquareMatrix class
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "squarematrix.hpp"
#include <iostream>

TEST_CASE("SquareMatrix tests", "[SquareMatrix]") {
    SquareMatrix sm1(2,2,2,2);
    CHECK(sm1.toString() == "[[2,2][2,2]]");
    SquareMatrix sm2;
    CHECK(sm2.toString() == "[[0,0][0,0]]");
    SquareMatrix sm3(sm1);
    CHECK(sm3.toString() == "[[2,2][2,2]]");
    sm1*=sm3;
    CHECK( sm1.toString() == "[[8,8][8,8]]" );
    sm1+=sm3;
    CHECK( sm1.toString() == "[[10,10][10,10]]" );
    sm1-=sm3;
    CHECK( sm1.toString() == "[[8,8][8,8]]" );
    std::stringstream strm;
    sm1.print(strm);
    CHECK(strm.str() == "[[8,8][8,8]]");
}