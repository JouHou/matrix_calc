/**
    \file intelement.cpp
    \brief Source code for IntElement class and class-related overloaded operators
*/
#include "intelement.hpp"
#include <sstream>

IntElement::IntElement() : IntElement{0} {}
IntElement::IntElement(int x) : val(x) {}
int IntElement::getVal() const {return val;}
void IntElement::setVal(int x) {val = x;}
IntElement& IntElement::operator += (const IntElement& elem) {
    val = val + elem.val;
    return *this;
}
IntElement& IntElement::operator -= (const IntElement& elem) {
    val = val - elem.val;
    return *this;
}
IntElement& IntElement::operator *= (const IntElement& elem) {
    val = val * elem.val;
    return *this;
}
IntElement operator+(const IntElement& elem1, const IntElement& elem2) {
    IntElement elem(elem1);
    elem += elem2;
    return elem;
}
IntElement operator-(const IntElement& elem1, const IntElement& elem2) {
    IntElement elem(elem1);
    elem -= elem2;
    return elem;
}
IntElement operator*(const IntElement& elem1, const IntElement& elem2) {
    IntElement elem(elem1);
    elem *= elem2;
    return elem;
}
std::string IntElement::toString() const {
    std::stringstream strm;
    strm << val;
    return strm.str();
}
std::ostream& operator<<(std::ostream& os, const IntElement& elem) {
    os << elem.toString();
    return os;
}