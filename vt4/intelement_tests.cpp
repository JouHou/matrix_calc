/**
    \file intelement_tests.cpp
    \brief Tests for IntElement class
*/
//#define CATCH_CONFIG_MAIN // commented out for independent testing of classes
#include "catch.hpp"
#include "intelement.hpp"
#include <sstream>

TEST_CASE("IntElement tests", "[IntElement]") {
    IntElement ie(2);
    CHECK(ie.getVal() == 2);
    
    ie.setVal(3);
    CHECK(ie.getVal() == 3);
    CHECK(ie.toString() == "3");
    
    IntElement ie2;
    ie += ie2;
    CHECK(ie.getVal() == 3);
    
    ie2.setVal(5);
    ie -= ie2;
    CHECK(ie.getVal() == -2);
    
    ie *= ie;
    CHECK(ie.getVal() == 4);
    
    IntElement ie3 = ie + ie2;
    CHECK(ie3.getVal() == 9);
    
    ie3 = ie3 - ie2;
    CHECK(ie3.getVal() == 4);
    
    ie3.setVal(2);
    ie2.setVal(6);
    IntElement ie4 = ie3 * ie2;
    CHECK(ie4.getVal() == 12);

    std::stringstream strm;
    strm << ie4;
    CHECK(strm.str() == "12");

    IntElement ie5{12};
    CHECK(ie5 == ie4);

    IntElement ie6;
    ie6 = *ie5.clone();
    CHECK(ie5 == ie6);

}