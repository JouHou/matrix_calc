/**
    \file squarematrix_tests.cpp
    \brief Tests for SquareMatrix class
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "squarematrix.hpp"

TEST_CASE("SquareMatrix constructor tests, Correct", "[SquareMatrix, correct]") {

    /* Test default constructor */
    SquareMatrix sm1;
    CHECK(sm1.getN() == 0);
    CHECK(sm1.toString() == "[]");

    /* Test paramatric constructor with different matrix sizes */
    CHECK_NOTHROW(SquareMatrix("[]"));
    CHECK_NOTHROW(SquareMatrix("[[0]]"));
    CHECK_NOTHROW(SquareMatrix("[[2]]"));
    CHECK_NOTHROW(SquareMatrix("[[-2]]"));
    CHECK_NOTHROW(SquareMatrix("[[1,2][1,2]]"));
    CHECK_NOTHROW(SquareMatrix("[[1,2,3][1,2,3][1,2,3]]"));
    CHECK_NOTHROW(SquareMatrix("[[1,2,3,4][1,2,3,4][1,2,3,4][1,2,3,4]]"));
    CHECK_NOTHROW(SquareMatrix("[[1,2,3,4,5][1,2,3,4,5][1,2,3,4,5][1,2,3,4,5][1,2,3,4,5]]"));
    SquareMatrix sm2("[[2,2][2,2]]");
    CHECK(sm2.toString() == "[[2,2][2,2]]");

    /* Test copy constructor */
    SquareMatrix sm3(sm2);
    CHECK(sm2.toString() == sm3.toString());

    /* Test move copy constructor */
    SquareMatrix sm4(std::move(sm2));
    CHECK(sm4.toString() == "[[2,2][2,2]]");
}

TEST_CASE("SquareMatrix parametric constructor tests, Incorrect size", "[SquareMatrix, incorrect]") {

    /* Test different kinds of wrong sizes */
    CHECK_THROWS_AS(SquareMatrix("[[1,2,3][1,2]]"), std::invalid_argument);
    CHECK_THROWS_AS(SquareMatrix("[[1,2,3][1,2,3]]"), std::invalid_argument);
}

TEST_CASE("SquareMatrix parametric constructor tests, bracket problems", "[SquareMatrix, incorrect]") {
    CHECK_THROWS(SquareMatrix("[1,2]")); // missing inner brackets
    CHECK_THROWS(SquareMatrix("[[1}}")); // non-matching brackets
    CHECK_THROWS(SquareMatrix("[[1,2][1,2]")); // missing closing bracket
    CHECK_THROWS(SquareMatrix("((1,2)(1,2))")); // wrong kind of brackets
}

TEST_CASE("SquareMatrix parametric constructor tests, Malformed input", "[SquareMatrix, incorrect]") {

    /* Test different kinds of malformed inputs */
    CHECK_THROWS(SquareMatrix("asdf")); // pure garbage
    CHECK_THROWS(SquareMatrix("[[|]]")); // pure garbage
    CHECK_THROWS(SquareMatrix("[[1,][1,]]")); // missing numbers
    CHECK_THROWS(SquareMatrix("[[1,,2][1,,2]]")); // extra commas
    CHECK_THROWS(SquareMatrix("[[1a,2a][1a,2a]]")); // useless alphabets
    CHECK_THROWS(SquareMatrix("[[1.0]]")); //  correct size, but float instead of int
    CHECK_THROWS(SquareMatrix("[[2,2][2,2]]aa")); // correct matrix, but excess garbage in the tail

    /* Test that thrown errors are of right type */
    CHECK_THROWS_AS(SquareMatrix("[[2,2][2,2]]aa"), std::invalid_argument);
    CHECK_THROWS_WITH(SquareMatrix("[[2,2][2,2]]aa"), "Not a square matrix!");
}

TEST_CASE("SquareMatrix output tests", "[SquareMatrix]") {

    /* Test function toString() */
    SquareMatrix sm1("[[2,2][2,2]]");
    CHECK(sm1.toString() == "[[2,2][2,2]]");

    /* Test function getN() */
    CHECK(sm1.getN() == 2);
    
    /* Test function print() */
    std::stringstream strm;
    sm1.print(strm);
    CHECK(strm.str() == "[[2,2][2,2]]");
    
    /* Test operator << */
    strm.str("");
    strm << sm1;
    CHECK(strm.str() == "[[2,2][2,2]]");
}

TEST_CASE("SquareMatrix assignment operator tests", "[SquareMatrix]") {

    SquareMatrix sm1("[[2,2][2,2]]");
    SquareMatrix sm2("[[1,1][1,1]]");
    SquareMatrix sm3("[[1,1,1][1,1,1][1,1,1]]");

    /* Test copy assignment operator */
    sm1 = sm3;
    CHECK(sm1==sm3);

    /* Test move assignment operator */
    sm1 = std::move(sm2);
    CHECK(sm1.toString() == "[[1,1][1,1]]");
}

TEST_CASE("SquareMatrix compound assignment operator tests", "[SquareMatrix]") {

    /* Test operator += */
    SquareMatrix sm1("[[2,2][2,2]]");
    SquareMatrix sm2(sm1);
    CHECK((sm1+=sm2).toString() == "[[4,4][4,4]]");
    
    /* Test operator -= */
    SquareMatrix sm3("[[1,1][1,1]]");
    CHECK((sm1-=sm3).toString() == "[[3,3][3,3]]");

    /* Test operator *= */
    SquareMatrix sm4("[[1,2,3][4,5,6][7,8,9]]");
    SquareMatrix sm5("[[1,2,3][4,5,6][7,8,9]]");
    CHECK((sm4*=sm5).toString() == "[[30,36,42][66,81,96][102,126,150]]");

    /* Test operations with different-sized matrices - should throw exception */
    CHECK_THROWS_AS(sm1-=sm4, std::domain_error);
    CHECK_THROWS_AS(sm1+=sm4, std::domain_error);
    CHECK_THROWS_AS(sm4*=sm3, std::domain_error);
}

TEST_CASE("SquareMatrix transpose test", "[SquareMatrix]") {

    /* Test function transpose() */
    SquareMatrix sm1("[[1,2,3][4,5,6][7,8,9]]");
    CHECK(sm1.transpose().toString() == "[[1,4,7][2,5,8][3,6,9]]");
}

TEST_CASE("SquareMatrix binary operator tests", "[SquareMatrix]") {

    /* Test operator == */
    SquareMatrix sm1("[[2,2][2,2]]");
    SquareMatrix sm2(sm1);
    CHECK(sm2==sm1);
    SquareMatrix sm3("[[1,1,1][1,1,1][1,1,1]]");
    SquareMatrix sm4("[[2,1,1][1,1,1][1,1,1]]");
    CHECK_FALSE(sm3==sm4);
    CHECK_FALSE(sm1==sm3);
    
    /* Test operators +, - and * */
    SquareMatrix sm5("[[1,2,3][4,5,6][7,8,9]]");
    CHECK((sm4 + sm5).toString() == "[[3,3,4][5,6,7][8,9,10]]");
    CHECK((sm5 - sm4).toString() == "[[-1,1,2][3,4,5][6,7,8]]");
    CHECK((sm5 * sm5).toString() == "[[30,36,42][66,81,96][102,126,150]]");

    /* Test that different-sized operands cause exception */
    CHECK_THROWS_AS(sm5+sm1, std::domain_error);
    CHECK_THROWS_AS(sm5-sm1, std::domain_error);
    CHECK_THROWS_AS(sm5*sm1, std::domain_error);
}
