/**
    \file intelement.cpp
    \brief Source code for IntElement class and class-related overloaded operators
*/
#include "intelement.hpp"
#include <sstream>
#include <memory>

IntElement::IntElement() : IntElement{0} {}
IntElement::IntElement(int x) : val(x) {}
int IntElement::getVal() const {return val;}
void IntElement::setVal(int x) {val = x;}
std::unique_ptr<IntElement> IntElement::clone() {
    return std::unique_ptr<IntElement>(new IntElement(this->val));
}
IntElement& IntElement::operator += (const IntElement& elem) {
    val = val + elem.val;
    return *this;
}
IntElement& IntElement::operator -= (const IntElement& elem) {
    val = val - elem.val;
    return *this;
}
IntElement& IntElement::operator *= (const IntElement& elem) {
    val = val * elem.val;
    return *this;
}
bool IntElement::operator == (const IntElement& i) const {
    return val == i.val;
}
std::string IntElement::toString() const {
    std::stringstream strm;
    strm << val;
    return strm.str();
}
IntElement operator+(const IntElement& elem1, const IntElement& elem2) {
    IntElement elem(elem1);
    elem += elem2;
    return elem;
}
IntElement operator-(const IntElement& elem1, const IntElement& elem2) {
    IntElement elem(elem1);
    elem -= elem2;
    return elem;
}
IntElement operator*(const IntElement& elem1, const IntElement& elem2) {
    IntElement elem(elem1);
    elem *= elem2;
    return elem;
}
std::ostream& operator<<(std::ostream& os, const IntElement& elem) {
    os << elem.toString();
    return os;
}