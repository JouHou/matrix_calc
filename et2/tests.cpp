#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <string>
#include "greeter.hpp"

TEST_CASE("Greeter test one","[correct]"){
    Greeter greeter("Test 1");
    CHECK(greeter.sayHello() == "Test 1");
}

TEST_CASE("Greeter test two","[incorrect]"){
    Greeter greeter("Test 2");
    CHECK_FALSE(greeter.sayHello() != "Test 2");
}
