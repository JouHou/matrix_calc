#include <string>
#include "greeter.hpp"

Greeter::Greeter(const std::string& str) {
    greetings = str;
}

std::string Greeter::sayHello() {
    return greetings;
}