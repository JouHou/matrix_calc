#ifndef GREETER_H_INCLUDED
#define GREETER_H_INCLUDED
#include <string>

class Greeter {
    private:
        std::string greetings;
    public:
        Greeter(const std::string&);
        std::string sayHello();
};
#endif
