#ifndef GREETER_H_INCLUDED
#define GREETER_H_INCLUDED
#include <vector>
#include <string>

class Greeter {
    private:
        std::vector<std::string*> greetings;
    public:
        Greeter(const std::string& g);
        Greeter(const Greeter& other);
        Greeter(Greeter&& other);
        virtual ~Greeter();
        Greeter& operator=(const Greeter& other);
        Greeter& operator=(Greeter&& other);
        void addGreet(const std::string& g);
        std::string sayHello() const;
};
#endif