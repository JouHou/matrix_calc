/**
    \file greeter_tests.cpp
    \brief Source code for greeter unit tests
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "greeter.hpp"


TEST_CASE("Greeter tests","[correct]"){
    Greeter grtr{"jou"};
    CHECK(grtr.sayHello() == "jou"); // test the parametric constructor
    grtr.addGreet("man");
    CHECK(grtr.sayHello() == "jou\nman"); // test the addGreet() function
    Greeter grtr2(grtr);
    CHECK(grtr2.sayHello() == "jou\nman"); // test the copy constructor
    Greeter grtr3(std::move(grtr2));
    CHECK(grtr3.sayHello() == "jou\nman"); // test the move copy constructor
    CHECK_FALSE(grtr2.sayHello() == "jou\nman"); // test that grtr2 is properly moved
    grtr2 = grtr3;
    CHECK(grtr2.sayHello() == "jou\nman"); // test the copy assignment operator
    grtr3 = std::move(grtr2);
    CHECK(grtr3.sayHello() == "jou\nman"); // test the move assignment operator
    CHECK(grtr2.sayHello() == ""); // test that grtr2 is properly moved
}
