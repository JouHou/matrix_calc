/**
    \file squarematrix.hpp
    \brief Header for SquareMatrix class
*/
#ifndef SQUAREMATRIX_H_INCLUDED
#define SQUAREMATRIX_H_INCLUDED
#include <ostream>
#include <vector>
#include <algorithm>
#include "intelement.hpp"

/**
    \class SquareMatrix
    \brief A class for n x n square matrices
*/
class SquareMatrix {
    private:
        int n;
        std::vector<std::vector<IntElement>> elements;
    public:
        /**
            \brief Default constructor
        */
        SquareMatrix();

        /**
            \brief Parametric constructor
            \param s The matrix given as a string
            \throw std::invalid_argument if the given string is not a proper square matrix 
        */        
        SquareMatrix(const std::string& s);

        /**
            \brief Copy constructor
            \param m The matrix to be copied
        */
        SquareMatrix(const SquareMatrix& m);

        /**
            \brief Default destructor
        */
        virtual ~SquareMatrix()=default;

        /**
            \brief Overloaded operator =
            \param m SquareMatrix to be assigned
            \return The resulting SquareMatrix
        */
        SquareMatrix& operator = (const SquareMatrix& m);

        /**
            \brief Transpose function
            \return Transposed copy of the squarematrix
        */
        SquareMatrix transpose() const;

        /**
            \brief Overloaded operator +=
            \param m SquareMatrix to be added
            \return The resulting SquareMatrix as a reference
        */
        SquareMatrix& operator += (const SquareMatrix& m);

        /**
            \brief Overloaded operator -=
            \param m SquareMatrix to be subtracted
            \return The resulting SquareMatrix as a reference
            \throw std::invalid_argument if the given matrix is not of right size
        */
        SquareMatrix& operator -= (const SquareMatrix& m);

        /**
            \brief Overloaded operator *=
            \param m SquareMatrix to be multiplied
            \return The resulting SquareMatrix as a reference
            \throw std::invalid_argument if the given matrix is not of right size
        */
        SquareMatrix& operator *= (const SquareMatrix& m);

        /**
            \brief Overloaded operator ==
            \param m SquareMatrix to be compared
            \return Boolean value
            \throw std::invalid_argument if the given matrix is not of right size
        */
        bool operator == (const SquareMatrix& m) const;

        /**
            \brief Print function for IntElement class, outputs to std::ostream
        */
        void print(std::ostream& os) const;

        /**
            \brief Function to transform the SquareMatrix to a string
            \return The resulting string
        */
        std::string toString() const;

        /**
            \brief Getter for param n
            \return value of n (int)
        */
        int getN() const;
        
        /**
            \brief Function to check if given matrix is a valid square matrix
            \return A boolean value
        */
        bool isSquareMatrix(const std::string&);
};

/**
    \brief Overloaded operator + (addition) for two SquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting SquareMatrix as a copy
    \throw std::domain_error if the given matrices are not of same size
*/
SquareMatrix operator+(const SquareMatrix& sm1, const SquareMatrix& sm2);

/**
    \brief Overloaded operator - (subtraction) for two SquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting SquareMatrix as a copy
    \throw std::domain_error if the given matrices are not of same size

*/
SquareMatrix operator-(const SquareMatrix& sm1, const SquareMatrix& sm2);

/**
    \brief Overloaded operator * (multiplication) for two SquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting SquareMatrix as a copy
    \throw std::domain_error if the given matrices are not of same size
*/
SquareMatrix operator*(const SquareMatrix& sm1, const SquareMatrix& sm2);

/**
    \brief Overloaded operator << (addition) for two SquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting SquareMatrix as a copy
*/
std::ostream& operator<<(std::ostream& os, const SquareMatrix& sm);
#endif
