/**
    \file squarematrix_tests.cpp
    \brief Tests for SquareMatrix class
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "squarematrix.hpp"

TEST_CASE("SquareMatrix class test part one", "[SquareMatrix]") {
    SquareMatrix sm1("[[2,2][2,2]]");
    CHECK(sm1.toString() == "[[2,2][2,2]]");
    CHECK(sm1.getN() == 2);

    std::stringstream strm;
    sm1.print(strm);
    CHECK(strm.str() == "[[2,2][2,2]]");
    strm.str("");
    strm << sm1;
    CHECK(strm.str() == "[[2,2][2,2]]");

    SquareMatrix sm2;
    CHECK(sm2.getN() == 0);
    CHECK(sm2.toString() == "[]");
    SquareMatrix sm3(sm1);
    CHECK(sm1.toString() == sm3.toString());
    sm1+=sm3;
    CHECK(sm1.toString() == "[[4,4][4,4]]");

    SquareMatrix sm4("[[1,1][1,1]]");
    sm1-=sm4;
    CHECK(sm1.toString() == "[[3,3][3,3]]");
    SquareMatrix sm5("[[1,1,1][1,1,1][1,1,1]]");
    CHECK_THROWS(sm1-=sm5);
    CHECK_THROWS(sm1+=sm5);
    sm1 = sm5;
    CHECK(sm1==sm5);
    CHECK_FALSE(sm3==sm5);
    SquareMatrix sm6("[[2,1,1][1,1,1][1,1,1]]");
    CHECK_FALSE(sm5==sm6);

    SquareMatrix sm7("[[1,2,3][4,5,6][7,8,9]]");
    SquareMatrix sm8("[[1,2,3][4,5,6][7,8,9]]");
    SquareMatrix sm9 = sm8.transpose();
    CHECK(sm9.toString() == "[[1,4,7][2,5,8][3,6,9]]");

    sm7*=sm8;
    CHECK(sm7.toString() == "[[30,36,42][66,81,96][102,126,150]]");
    CHECK_THROWS(sm7*=sm4);

    sm7 = sm6 + sm8;
    CHECK(sm7.toString() == "[[3,3,4][5,6,7][8,9,10]]");
    sm7 = sm8 - sm6;
    CHECK(sm7.toString() == "[[-1,1,2][3,4,5][6,7,8]]");
    sm7 = sm8 * sm8;
    CHECK(sm7.toString() == "[[30,36,42][66,81,96][102,126,150]]");
    CHECK_THROWS_AS(sm7+sm4, std::domain_error);
    CHECK_THROWS(sm7-sm4);
    CHECK_THROWS(sm7*sm4);
    

    CHECK_THROWS(SquareMatrix("[[2,2][2,2]]aa"));
    CHECK_THROWS(SquareMatrix("[[2,2,2][2,2,2]]"));
    CHECK_THROWS(SquareMatrix("[1."));
    CHECK_THROWS_AS(SquareMatrix("[[2,2][2,2]]aa"), std::invalid_argument);
    CHECK_THROWS_WITH(SquareMatrix("[[2,2][2,2]]aa"), "Not a square matrix!");
    CHECK_THROWS(SquareMatrix("[[1."));

    CHECK_NOTHROW(SquareMatrix("[[1,2,3][1,2,3][1,2,3]]"));
    CHECK_NOTHROW(SquareMatrix("[]"));
    CHECK_NOTHROW(SquareMatrix("[[1,2][1,2]]"));
    CHECK_NOTHROW(SquareMatrix("[[2]]"));
    CHECK_NOTHROW(SquareMatrix("[[-2]]"));
    CHECK_NOTHROW(SquareMatrix("[[0]]"));
    CHECK_NOTHROW(SquareMatrix("[[1,2,3,4][1,2,3,4][1,2,3,4][1,2,3,4]]"));

    CHECK_THROWS(SquareMatrix("[1,2]"));
    CHECK_THROWS(SquareMatrix("[1,2a"));
    CHECK_THROWS(SquareMatrix("[1}"));
    CHECK_THROWS(SquareMatrix("[1,]"));
    CHECK_THROWS(SquareMatrix("[a1,2]"));
    CHECK_THROWS(SquareMatrix("[[|]]"));
    CHECK_THROWS(SquareMatrix("[a1,a2]"));
    CHECK_THROWS(SquareMatrix("[[1a,2a][1a,2a]]"));
    CHECK_THROWS(SquareMatrix("[1,2]aa151gg13"));
    CHECK_THROWS(SquareMatrix("[[1]]aa151gg13"));
    CHECK_THROWS(SquareMatrix("[1,2]"));
    CHECK_THROWS(SquareMatrix("[1,,,,,,2]"));
    CHECK_THROWS(SquareMatrix("[[1,2,3][1,2,3]]"));
    CHECK_THROWS(SquareMatrix("[[1,2,3][1,2]]"));
    CHECK_THROWS(SquareMatrix("[[1,2][1,2]"));
    CHECK_THROWS(SquareMatrix("((1,2)(1,2))"));
    CHECK_THROWS(SquareMatrix("asdf"));
    CHECK_THROWS(SquareMatrix(" [[1,2][1,2]"));
}