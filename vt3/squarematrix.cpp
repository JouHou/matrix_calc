/**
    \file squarematrix.cpp
    \brief Source code for SquareMatrix class
*/
#include "squarematrix.hpp"
#include "intelement.hpp"
#include <sstream>
#include <algorithm>
#include <stdexcept>

SquareMatrix::SquareMatrix() {
    n = 0;
    elements = {};
}

SquareMatrix::SquareMatrix(const std::string& s) {
    if(!isSquareMatrix(s)) 
        throw std::invalid_argument("Not a square matrix!");
}

SquareMatrix::SquareMatrix(const SquareMatrix& m) {
    elements = m.elements;
    n = m.n;
}

SquareMatrix& SquareMatrix::operator = (const SquareMatrix& m) {
    elements = m.elements;
    n = m.n;
    return *this;
}

SquareMatrix SquareMatrix::transpose() const {
    SquareMatrix tmpMtrx(*this);

    for (int i=0;i<n;i++) {
        for (int j=0;j<n;j++) {
            tmpMtrx.elements[i][j] = elements[j][i];
        }
    }
    return tmpMtrx;
}

SquareMatrix& SquareMatrix::operator += (const SquareMatrix& m) {
    if (n != m.n) {
        throw std::invalid_argument("Matrices must be of the same size!");
    } else {
        auto iter2 = m.elements.begin();

        for(auto iter = elements.begin(); iter!=elements.end();iter++) {
            std::transform(iter->begin(),iter->end(),iter2->begin(),iter->begin(),std::plus<IntElement>());
            iter2++;
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator -= (const SquareMatrix& m) {
    if (n != m.n) {
        throw std::invalid_argument("Matrices must be of the same size!");
    } else {
        auto iter2 = m.elements.begin();
        
        for(auto iter = elements.begin(); iter!=elements.end();iter++) {
            std::transform(iter->begin(),iter->end(),iter2->begin(),iter->begin(),std::minus<IntElement>());
            iter2++;
        }
    }
    return *this;
}

SquareMatrix& SquareMatrix::operator *= (const SquareMatrix& m) {
    SquareMatrix tmpMtrx(*this);
    if (n != m.n) {
        throw std::invalid_argument("Matrices must be of the same size!");
    } else {
        for (int i=0;i<n;i++) { // row
            for (int j=0;j<n;j++) {  // column
                for (int k = 0;k<n;k++) {
                    elements[i][j]+=tmpMtrx.elements[i][k]*m.elements[k][j];
                }
            }
        }
    }
    *this-=tmpMtrx;
    return *this;
}

bool SquareMatrix::operator == (const SquareMatrix& m) const {
    if (n != m.n) {
        return false;
    }
    return (toString() == m.toString());
}

void SquareMatrix::print(std::ostream& os) const {
    os << toString();
}

std::string SquareMatrix::toString() const {
    std::stringstream strm;
    strm << "[";
    for (auto row : elements) {
        strm << "[";
        bool first = true;
        auto prnt = [&first,&strm](IntElement elem){ if (!first) {strm << ",";} strm << elem; first = false;}; // lambda
        std::for_each(row.begin(),row.end(), prnt);
        strm << "]";
    }
    strm << "]";

    return strm.str();
}

int SquareMatrix::getN() const {
    return n;
}

bool SquareMatrix::isSquareMatrix(const std::string& str) {
    std::stringstream input(str);
    char c;
    int number;
    int rivit = 0;
    int sarakkeet = 0;
    int sarakkeet_temp = 0;

    input >> c;
    
    if(!input.good() || c!='[') {
        return false;
    }

    input >> c;

    while(input.good() && c != ']') {
        if(input.good() && c == '[') {
            rivit++;
            elements.push_back({});
            do {
                input >> number;
                if (input.fail()) {
                    return false;
                }
                elements.back().push_back(IntElement(number));
                sarakkeet_temp++;
                input >> c;
                if (c != ',' && c != ']') {
                    return false;
                }
            } while (c != ']');

            if (sarakkeet == 0) {
                sarakkeet = sarakkeet_temp;
            } else {
                if (sarakkeet_temp != sarakkeet) {
                    return false;                
                }
            }

            sarakkeet_temp = 0;
        } else {
            return false;
        }
        input >> c;
        if (input.eof()) {
            return false;
        }
    }

    std::string tail;
    input >> tail;
    if (tail!="") {
        return false;
    }

    if (rivit == sarakkeet) {
        n = rivit;
        return true;
    } else {
        return false;
    }
}

SquareMatrix operator+(const SquareMatrix& sm1, const SquareMatrix& sm2) {
    if (sm1.getN() != sm2.getN()) {
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    SquareMatrix tmpMtrx(sm1);
    tmpMtrx+=sm2;
    return tmpMtrx;
}

SquareMatrix operator-(const SquareMatrix& sm1, const SquareMatrix& sm2) {
    if (sm1.getN() != sm2.getN()) {
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    SquareMatrix tmpMtrx(sm1);
    tmpMtrx-=sm2;
    return tmpMtrx;
}

SquareMatrix operator*(const SquareMatrix& sm1, const SquareMatrix& sm2) {
    if (sm1.getN() != sm2.getN()) {
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    SquareMatrix tmpMtrx(sm1);
    tmpMtrx*=sm2;
    return tmpMtrx;
}

std::ostream& operator<<(std::ostream& os, const SquareMatrix& sm) {
    os << sm.toString();
    return os;
}