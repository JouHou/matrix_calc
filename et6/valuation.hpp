/**
    \file valuation.hpp
    \brief Header for Valuation class
*/
#ifndef VALUATION_H_INCLUDED
#define VALUATION_H_INCLUDED
#include <map>
/**
    \class Valuation
    \brief A class for valuation objects
*/
using Valuation = std::map<char,int>;
#endif