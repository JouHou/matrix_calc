/**
    \file compositeelement.hpp
    \brief Header for CompositeElement class
*/
#ifndef COMPOSITEELEMENT_H_INCLUDED
#define COMPOSITEELEMENT_H_INCLUDED
#include <string>
#include <memory>
#include <functional>
#include "valuation.hpp"
#include "element.hpp"

/**
    \class CompositeElement
    \brief A class for composite elements
*/      
class CompositeElement {
    private:
        Element* oprnd1;
        Element* oprnd2;
        std::function<int(int,int)> op_fun;
        char op_ch;
    public:

        /**
            \brief Construct a new Composite Element object from parameters
            \param e1 Element&
            \param e2 Element&
            \param op std::function<int(int,int)>
            \param opc char
         */
        CompositeElement(const Element& e1, const Element& e2, const std::function<int(int,int)>& op, char opc);

        /**
            \brief Construct a new Composite Element object by copying         
            \param e CompositeElement& to be copied from
         */
        CompositeElement(const CompositeElement& e);

        /**
            \brief Copy assignment operator =
            \param e CompositeElement& to be copied from
            \return CompositeElement& 
         */
        CompositeElement& operator=(const CompositeElement& e);
        
        /**
            \brief Destroy the Composite Element object
         */
        virtual ~CompositeElement(){};

        /**
            \brief Cloning function for Composite Elements
            \return Element*
         */
        Element* clone() const;

        /**
            \brief Function to transform Composite Element to string
            \return std::string
         */
        std::string toString() const;
        
        /**
            \brief Evaluate function for Composite Elements
            \param v Valuation&
            \return int
         */
        int evaluate(const Valuation& v) const;
};

#endif
