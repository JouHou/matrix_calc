/**
    \file symbolicmatrix_tests.cpp
    \brief Tests for SymbolicSquareMatrix class
*/
#include <sstream>
#include "catch.hpp"
#include "concretematrix.hpp"
#include "valuation.hpp"
#include "symbolicmatrix.hpp"

TEST_CASE("SymbolicSquareMatrix default constructor test", "[SymbolicSquareMatrix, correct]") {
    SymbolicSquareMatrix sm;
    CHECK(sm.getN() == 0);
}

TEST_CASE("SymbolicSquareMatrix parametric constructor test", "[SymbolicSquareMatrix, correct]") {
    SymbolicSquareMatrix sm("[[x,1][1,y]]");
    CHECK(sm.getN() == 2);
    CHECK(sm.toString() == "[[x,1][1,y]]");

    // Test parametric constructor with different matrix sizes //
    CHECK_NOTHROW(SymbolicSquareMatrix("[]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[x]]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[-2]]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[1,z][1,2]]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[y,2,3,4][y,2,3,4][y,2,3,4][y,2,3,4]]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[1,2,t,4,5][1,2,t,4,5][1,2,3,4,t][1,t,3,4,5][1,2,3,4,t]]"));
}

TEST_CASE("SymbolicSquareMatrix evaluation test", "[SymbolicSquareMatrix, correct]") {
    Valuation valu;
    valu['x'] = 5;
    valu['y'] = 8;
    SymbolicSquareMatrix sm("[[x,1][1,y]]");
    CHECK(sm.toString() == "[[x,1][1,y]]");
    ConcreteSquareMatrix csm = sm.evaluate(valu);
    CHECK(csm.toString() == "[[5,1][1,8]]");

    SymbolicSquareMatrix sm1("[[z,1][1,y]]");
    CHECK_THROWS(sm1.evaluate(valu));
}

TEST_CASE("SymbolicSquarematrix copy constructor tests", "[SymbolicSquareMatrix, correct]") {
    SymbolicSquareMatrix sm2("[[2,2][2,2]]");
    SymbolicSquareMatrix sm3(sm2);
    CHECK(sm2.toString() == sm3.toString());
}

TEST_CASE("SymbolicSquarematrix move copy constructor tests", "[SymbolicSquareMatrix, correct]") {
    SymbolicSquareMatrix sm2("[[2,2][2,2]]");
    SymbolicSquareMatrix sm4(std::move(sm2));
    CHECK(sm4.toString() == "[[2,2][2,2]]");
}

TEST_CASE("SymbolicSquareMatrix parametric constructor tests, Incorrect size", "[ConcreteSquareMatrix, incorrect]") {
    CHECK_THROWS_AS(SymbolicSquareMatrix("[[1,2,3][1,2]]"), std::invalid_argument);
    CHECK_THROWS_AS(SymbolicSquareMatrix("[[1,2,3][1,2,3]]"), std::invalid_argument);
}

TEST_CASE("SymbolicSquareMatrix parametric constructor tests, bracket problems", "[ConcreteSquareMatrix, incorrect]") {
    CHECK_THROWS(SymbolicSquareMatrix("[x,2]")); // missing inner brackets
    CHECK_THROWS(SymbolicSquareMatrix("[[y}}")); // non-matching brackets
    CHECK_THROWS(SymbolicSquareMatrix("[[c,2][1,2]")); // missing closing bracket
    CHECK_THROWS(SymbolicSquareMatrix("((1,2)(1,a))")); // wrong kind of brackets
}

TEST_CASE("SymbolicSquareMatrix parametric constructor tests, Malformed input", "[ConcreteSquareMatrix, incorrect]") {

    // Test different kinds of malformed inputs //
    CHECK_THROWS(SymbolicSquareMatrix("[[")); // pure garbage
    CHECK_THROWS(SymbolicSquareMatrix("asdf")); // pure garbage
    CHECK_THROWS(SymbolicSquareMatrix("[[|]]")); // pure garbage
    CHECK_THROWS(SymbolicSquareMatrix("[[1,][1,]]")); // missing numbers
    CHECK_THROWS(SymbolicSquareMatrix("[[1,,2][1,,2]]")); // extra commas
    CHECK_THROWS(SymbolicSquareMatrix("[[1a,2a][1a,2a]]")); // useless alphabets
    CHECK_THROWS(SymbolicSquareMatrix("[[1.0]]")); //  correct size, but float instead of int
    CHECK_THROWS(SymbolicSquareMatrix("[[2,2][2,2]]aa")); // correct matrix, but excess garbage in the tail

    // Test that thrown errors are of right type //
    CHECK_THROWS_AS(SymbolicSquareMatrix("[[2,2][2,2]]aa"), std::invalid_argument);
    CHECK_THROWS_WITH(SymbolicSquareMatrix("[[2,2][2,2]]aa"), "Not a square matrix!");
}

TEST_CASE("SymbolicSquareMatrix transpose() test", "[SymbolicSquareMatrix]") {
    SymbolicSquareMatrix sm1("[[1,2,3][4,5,6][7,8,9]]");
    CHECK(sm1.transpose().toString() == "[[1,4,7][2,5,8][3,6,9]]");
}

TEST_CASE("SymbolicSquareMatrix move copy assignment operator = test", "[SymbolicSquareMatrix]") {
    SymbolicSquareMatrix sm("[[1,2,3][4,5,6][7,8,9]]");
    SymbolicSquareMatrix sm1;
    sm1 = std::move(sm);
    CHECK(sm1.toString() == "[[1,2,3][4,5,6][7,8,9]]");
}

TEST_CASE("SymbolicSquareMatrix copy assignment operator = test", "[SymbolicSquareMatrix]") {
    SymbolicSquareMatrix sm("[[1,2,3][4,5,6][7,8,9]]");
    SymbolicSquareMatrix sm1;
    sm1 = sm;
    CHECK(sm1.toString() == "[[1,2,3][4,5,6][7,8,9]]");
}

TEST_CASE("SymbolicSquareMatrix operator == test", "[SymbolicSquareMatrix]") {
    SymbolicSquareMatrix sm("[[1,2,3][4,5,6][7,8,9]]");
    SymbolicSquareMatrix sm1 = sm;
    SymbolicSquareMatrix sm2("[[1,3][4,6]]");
    CHECK(sm1 == sm);
    CHECK_FALSE(sm2 == sm);    
}

TEST_CASE("SymbolicSquareMatrix print() test", "[SymbolicSquareMatrix]") {
    std::stringstream strm;
    SymbolicSquareMatrix sm("[[1,2,3][4,5,6][7,8,9]]");
    sm.print(strm);
    CHECK(strm.str() == "[[1,2,3][4,5,6][7,8,9]]");
}

TEST_CASE("SymbolicSquareMatrix operator << test", "[SymbolicSquareMatrix]") {
    std::stringstream strm;
    SymbolicSquareMatrix sm("[[1,2,3][4,5,6][7,8,9]]");
    strm << sm;
    CHECK(strm.str() == "[[1,2,3][4,5,6][7,8,9]]");
}