/**
    \file concretematrix.hpp
    \brief Header for ConcreteSquareMatrix class
*/
#ifndef CONCRETESQUAREMATRIX_H_INCLUDED
#define CONCRETESQUAREMATRIX_H_INCLUDED
#include <ostream>
#include <vector>
#include <algorithm>
#include <memory>
#include "intelement.hpp"

class SymbolicSquareMatrix;

/**
    \class ConcreteSquareMatrix
    \brief A class for n x n concrete square matrices
*/
class ConcreteSquareMatrix {
    private:
        int n;
        std::vector<std::vector<std::unique_ptr<IntElement>>> elements;
        friend SymbolicSquareMatrix;
    public:

        /**
            \brief Default constructor
        */
        ConcreteSquareMatrix();

        /**
            \brief Parametric constructor
            \param str_m The matrix given as a string
            \throw std::invalid_argument if the given string is not a proper square matrix 
        */        
        ConcreteSquareMatrix(const std::string& str_m);

        /**
            \brief Copy constructor
            \param m The matrix to be copied
        */
        ConcreteSquareMatrix(const ConcreteSquareMatrix& m);

        /**
            \brief Move copy constructor
            \param m The matrix to be moved
        */
        ConcreteSquareMatrix(ConcreteSquareMatrix&& m);

        /**
            \brief Default destructor
        */
        virtual ~ConcreteSquareMatrix()=default;

        /**
            \brief Overloaded copy assignment operator =
            \param m ConcreteSquareMatrix to be assigned
            \return The resulting ConcreteSquareMatrix as a reference
        */
        ConcreteSquareMatrix& operator = (const ConcreteSquareMatrix& m);

        /**
            \brief Overloaded move assignment operator =
            \param m ConcreteSquareMatrix to be assigned
            \return The resulting ConcreteSquareMatrix as a reference
        */
        ConcreteSquareMatrix& operator = (ConcreteSquareMatrix&& m);

        /**
            \brief Transpose function
            \return Transposed copy of the ConcreteSquareMatrix
        */
        ConcreteSquareMatrix transpose() const;

        /**
            \brief Overloaded operator +=
            \param m ConcreteSquareMatrix to be added
            \return The resulting ConcreteSquareMatrix as a reference
            \throw std::invalid_argument if the given matrix is not of right size
        */
        ConcreteSquareMatrix& operator += (const ConcreteSquareMatrix& m);

        /**
            \brief Overloaded operator -=
            \param m ConcreteSquareMatrix to be subtracted
            \return The resulting ConcreteSquareMatrix as a reference
            \throw std::invalid_argument if the given matrix is not of right size
        */
        ConcreteSquareMatrix& operator -= (const ConcreteSquareMatrix& m);

        /**
            \brief Overloaded operator *=
            \param m ConcreteSquareMatrix to be multiplied
            \return The resulting ConcreteSquareMatrix as a reference
            \throw std::invalid_argument if the given matrix is not of right size
        */
        ConcreteSquareMatrix& operator *= (const ConcreteSquareMatrix& m);

        /**
            \brief Overloaded comparison operator ==
            \param m ConcreteSquareMatrix to be compared
            \return Boolean value
        */
        bool operator == (const ConcreteSquareMatrix& m) const;

        /**
            \brief Print function for ConcreteSquareMatrix class, outputs to std::ostream
            \param os reference to an ostream where the representation is wanted
        */
        void print(std::ostream& os) const;

        /**
            \brief Function to transform the ConcreteSquareMatrix to a string
            \return The resulting string as a copy
        */
        std::string toString() const;

        /**
            \brief Getter for param n
            \return value of n (int)
        */
        int getN() const;
        
        /**
            \brief Function to check if given matrix is a valid square matrix
            \param str_m A reference to std::string representation of a matrix
            \return A boolean value
        */
        bool isSquareMatrix(const std::string& str_m);
};

/**
    \brief Overloaded operator + (addition) for two ConcreteSquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting ConcreteSquareMatrix as a copy
    \throw std::domain_error if the given matrices are not of same size
*/
ConcreteSquareMatrix operator+(const ConcreteSquareMatrix& sm1, const ConcreteSquareMatrix& sm2);

/**
    \brief Overloaded operator - (subtraction) for two ConcreteSquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting ConcreteSquareMatrix as a copy
    \throw std::domain_error if the given matrices are not of same size

*/
ConcreteSquareMatrix operator-(const ConcreteSquareMatrix& sm1, const ConcreteSquareMatrix& sm2);

/**
    \brief Overloaded operator * (multiplication) for two ConcreteSquareMatrix objects
    \param sm1 Left hand side operand
    \param sm2 Right hand side operand
    \return The resulting ConcreteSquareMatrix as a copy
    \throw std::domain_error if the given matrices are not of same size
*/
ConcreteSquareMatrix operator*(const ConcreteSquareMatrix& sm1, const ConcreteSquareMatrix& sm2);

/**
    \brief Overloaded operator << (string insertion) for ConcreteSquareMatrix objects
    \param os std::ostream&
    \param sm const ConcreteSquareMatrix&
    \return The resulting ConcreteSquareMatrix as a copy
*/
std::ostream& operator<<(std::ostream& os, const ConcreteSquareMatrix& sm);
#endif
