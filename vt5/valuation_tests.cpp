/**
    \file valuation_tests.cpp
    \brief Tests for Valuation class
*/
#include "catch.hpp"
#include "varelement.hpp"
#include "valuation.hpp"

TEST_CASE("Valuation tests", "[ConcreteSquareMatrix, correct]") {
    Valuation valu;
    valu['x'] = 5;
    VariableElement varel{'x'};
    VariableElement varel2{'z'};
    CHECK(varel.evaluate(valu) == 5);
    CHECK_THROWS(varel2.evaluate(valu));
}