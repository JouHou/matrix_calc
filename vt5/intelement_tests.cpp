/**
    \file intelement_tests.cpp
    \brief Tests for IntElement class
*/
#include "catch.hpp"
#include "intelement.hpp"
#include <sstream>
#include <memory>

TEST_CASE("IntElement default constructor and getVal() test", "[IntElement]") {
    IntElement ie;
    CHECK(ie.getVal() == 0);
}

TEST_CASE("IntElement setVal() test", "[IntElement]") {
    IntElement ie;
    ie.setVal(3);
    CHECK(ie.getVal() == 3);
    CHECK(ie.toString() == "3");
}

TEST_CASE("IntElement operator += test", "[IntElement]") {
    IntElement ie{2};
    IntElement ie2{2};
    ie += ie2;
    CHECK(ie.getVal() == 4);
}
TEST_CASE("IntElement operator -= test", "[IntElement]") {
    IntElement ie{5};
    IntElement ie2{2};
    ie -= ie2;
    CHECK(ie.getVal() == 3);
}
TEST_CASE("IntElement operator *= test", "[IntElement]") {
    IntElement ie{5};
    ie *= ie;
    CHECK(ie.getVal() == 25);
}
TEST_CASE("IntElement operator + test", "[IntElement]") {
    IntElement ie{5};
    IntElement ie2{2};
    CHECK(ie + ie2 == 7);
}
TEST_CASE("IntElement operator - test", "[IntElement]") {
    IntElement ie{5};
    IntElement ie2{2};
    CHECK(ie - ie2 == 3);
}
TEST_CASE("IntElement operator * test", "[IntElement]") {
    IntElement ie{5};
    IntElement ie2{2};
    CHECK(ie * ie2 == 10);
}
TEST_CASE("IntElement operator << test", "[IntElement]") {
    IntElement ie{5};

    std::stringstream strm;
    strm << ie;
    CHECK(strm.str() == "5");
}
TEST_CASE("IntElement operator = test", "[IntElement]") {
    IntElement ie{5};

    IntElement ie2 = *std::unique_ptr<IntElement>(ie.clone());
    CHECK(ie == ie2);
}