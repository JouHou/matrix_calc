/**
    \file concretematrix.cpp
    \brief Source code for ConcreteSquareMatrix class
*/
#include "concretematrix.hpp"
#include "intelement.hpp"
#include "element.hpp"
#include <sstream>
#include <memory>
#include <algorithm>
#include <stdexcept>

ConcreteSquareMatrix::ConcreteSquareMatrix():elements{} { // default constructor, initialize to n=0 and empty vector
    n = 0;
}

ConcreteSquareMatrix::ConcreteSquareMatrix(const std::string& str_m) { // parametric constructor
    if(!isSquareMatrix(str_m)) // returns false str_m is not in a correct form
        throw std::invalid_argument("Not a square matrix!");
}

ConcreteSquareMatrix::ConcreteSquareMatrix(const ConcreteSquareMatrix& m) { // copy constructor
    for(auto& mrow : m.elements) {
        elements.push_back({}); // push back an empty row
        for(auto& melem : mrow) {
            elements.back().push_back(std::unique_ptr<IntElement>(melem->clone())); // push back a clone of the element
        }
    }
    n = m.n;
}

ConcreteSquareMatrix::ConcreteSquareMatrix(ConcreteSquareMatrix&& m) { // move copy constructor
    elements = std::move(m.elements);
    n = m.n;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator = (const ConcreteSquareMatrix& m) { // copy assignment operator
    if (this == &m) return *this; // if trying to assign onto itself

    ConcreteSquareMatrix tmpMtrx{m};
    std::swap(elements, tmpMtrx.elements);
    n = m.n;
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator = (ConcreteSquareMatrix&& m) { // move assignment operator
    if (this == &m) return *this; // if trying to assign onto itself

    elements = std::move(m.elements);
    n = m.n;
    return *this;
}

ConcreteSquareMatrix ConcreteSquareMatrix::transpose() const { // return a transposed copy of the matrix
    ConcreteSquareMatrix tmpMtrx(*this);

    for (int i=0;i<n;i++) { // row
        for (int j=0;j<n;j++) { // column
            *tmpMtrx.elements[i][j] = *elements[j][i]; // swap i,j -> j,i
        }
    }
    return tmpMtrx;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator += (const ConcreteSquareMatrix& m) { // compound assignment operator +=
    if (n != m.n) { // check that dimensions are same
        throw std::domain_error("Matrices must be of the same size!");
    } else {

        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                *elements[i][j] += *m.elements[i][j]; // assign to this[i][j]
            }
        }
    }
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator -= (const ConcreteSquareMatrix& m) { // compound assignment operator -=
    if (n != m.n) { // check that dimensions are same
        throw std::domain_error("Matrices must be of the same size!");
    } else {

        for(int i = 0; i < n; i++) { // row
            for(int j = 0; j < n; j++) { // column
                *elements[i][j] -= *m.elements[i][j]; // assign to this[i][j]
            }
        }
    }
    return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator *= (const ConcreteSquareMatrix& m) {
    ConcreteSquareMatrix tmpMtrx(*this);
    if (n != m.n) {
        throw std::domain_error("Matrices must be of the same size!");
    } else {
        for (int i=0;i<n;i++) { // row
            for (int j=0;j<n;j++) {  // column
                for (int k = 0;k<n;k++) {
                    *elements[i][j]+=*tmpMtrx.elements[i][k] * *m.elements[k][j]; // i,j = i,j + i,k * k,j  
                }
            }
        }
    }
    return *this-=tmpMtrx; // subtract the original matrix from the result here so no need for zero matrix
}

bool ConcreteSquareMatrix::operator == (const ConcreteSquareMatrix& m) const { // comparison operator
    if (n != m.n) {
        return false; // if dimensions don't match, clearly not equal
    }
    return (toString() == m.toString()); // equal matrices should transform into equal strings
}

void ConcreteSquareMatrix::print(std::ostream& os) const {
    os << toString();
}

std::string ConcreteSquareMatrix::toString() const { // return a string representation of the matrix
    std::stringstream strm;
    strm << "["; // beginning of matrix
    for (const auto& row : elements) { // row
        strm << "[";
        bool first = true;
        for (const auto& elem : row) { // element in the row
            if (!first) { // don't put a comma before the first number
                 strm << ",";
            } 
            strm << *elem;
            first = false;
        }
        strm << "]"; // end of row
    }
    strm << "]"; // end of matrix

    return strm.str();
}

int ConcreteSquareMatrix::getN() const { // getter for param n
    return n;
}

bool ConcreteSquareMatrix::isSquareMatrix(const std::string& str) { // check if the given string is proper square matrix
    std::stringstream input(str);
    char c;

    input >> c;
    
    if(!input.good() || c!='[') { // if the string is garbage in the beginning
        return false;
    }

    input >> c;

    int number;
    int rows = 0;
    int columns = 0;
    int columns_temp = 0;

    while(input.good() && c != ']') { // stay in the loop while stream is good and no closing bracket is detected
        if(input.good() && c == '[') { // starting bracket of the row
            rows++;
            elements.push_back({});
            do { // start looking for numbers
                input >> number;
                if (input.fail()) { // if there is no number right after starting bracket
                    return false;
                }
                elements.back().push_back(std::move(std::unique_ptr<IntElement>(new IntElement(number))));
                columns_temp++;
                input >> c;
                if (c != ',' && c != ']') { // if, after the number, the char isn't "," or "]", the input string is malformed
                    return false;
                }
            } while (c != ']'); // stay in the loop while closing bracket of the row is not detected

            if (columns == 0) { // assign the first row's column number as baseline
                columns = columns_temp;
            } else {
                if (columns_temp != columns) { // if any row differs from the first one in number of columns, the matrix isn't square matrix
                    return false;                
                }
            }

            columns_temp = 0;
        } else { // we haven't detected proper starting bracket of the row
            return false;
        }
        input >> c;
        if (input.eof()) { // if end of stream here, we haven't detected closign bracket, so the input string is malformed
            return false;
        }
    }

    std::string tail;
    input >> tail;
    if (tail!="") { // garbage after proper matrix
        return false;
    }

    if (rows == columns) { // if number of rows and columns match
        n = rows;
        return true;
    } else { // if number of rows and columns don't match, no square matrix
        return false;
    }
}

ConcreteSquareMatrix operator+(const ConcreteSquareMatrix& sm1, const ConcreteSquareMatrix& sm2) {
    if (sm1.getN() != sm2.getN()) { // if matrices are not same-sized, throw exception
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    ConcreteSquareMatrix tmpMtrx(sm1);
    tmpMtrx+=sm2;
    return tmpMtrx;
}

ConcreteSquareMatrix operator-(const ConcreteSquareMatrix& sm1, const ConcreteSquareMatrix& sm2) {
    if (sm1.getN() != sm2.getN()) { // if matrices are not same-sized, throw exception
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    ConcreteSquareMatrix tmpMtrx(sm1);
    tmpMtrx-=sm2;
    return tmpMtrx;
}

ConcreteSquareMatrix operator*(const ConcreteSquareMatrix& sm1, const ConcreteSquareMatrix& sm2) {
    if (sm1.getN() != sm2.getN()) { // if matrices are not same-sized, throw exception
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    ConcreteSquareMatrix tmpMtrx(sm1);
    tmpMtrx*=sm2;
    return tmpMtrx;
}

std::ostream& operator<<(std::ostream& os, const ConcreteSquareMatrix& sm) {
    os << sm.toString();
    return os;
}
