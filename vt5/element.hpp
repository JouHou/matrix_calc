/**
    \file element.hpp
    \brief Header for Element class
*/
#ifndef ELEMENT_H_INCLUDED
#define ELEMENT_H_INCLUDED
#include <string>
#include <memory>
#include "valuation.hpp"

/**
    \class Element
    \brief An interface class for single matrix elements
*/
class Element {
    public:

        /**
            \brief Default destructor
        */
        virtual ~Element(){};

        /**
            \brief Clone function for elements
            \return Pointer to the cloned element
        */
        virtual Element* clone() const = 0;

        /**
            \brief toString function for elements
            \return Copy of resulting string
        */
        virtual std::string toString() const = 0;

        /**
            \brief Evaluation function for elements
            \param val Valuation map
            \return Int
            \throw If called on VariableElement, throws if val doesn't have matching variable
        */
        virtual int evaluate(const Valuation& val) const = 0;
};

/**
    \brief Overloaded operator << for Element objects
    \param os Output stream
    \param e Element to be output to the stream
    \return Output stream
*/
std::ostream& operator<<(std::ostream& os, const Element& e);

#endif
