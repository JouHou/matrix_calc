/**
    \file varelement.cpp
    \brief Source code for VariableElement class
*/
#include <iostream>
#include <sstream>
#include <memory>
#include "valuation.hpp"
#include "element.hpp"
#include "varelement.hpp"

VariableElement::VariableElement() {
    val = 0;
}

VariableElement::VariableElement(char v) {
    val = v;
}

char VariableElement::getVal() const {
    return val;
}

void VariableElement::setVal(char v) {
    val = v;
}

VariableElement* VariableElement::clone() const {
    return new VariableElement(val);
}   

std::string VariableElement::toString() const {
    return std::string(1,val);
}

int VariableElement::evaluate(const Valuation& v) const {
    int x = v.at(val);
    return x;
}

bool VariableElement::operator == (const VariableElement& i) const {
    return val == i.val;
}