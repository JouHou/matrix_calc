/**
    \file symbolicmatrix.hpp
    \brief Header for SymbolicSquareMatrix class
*/
#ifndef SYMBOLICSQUAREMATRIX_H_INCLUDED
#define SYMBOLICSQUAREMATRIX_H_INCLUDED
#include <ostream>
#include <vector>
#include <algorithm>
#include <memory>
#include "intelement.hpp"
#include "concretematrix.hpp"
#include "element.hpp"

/**
    \class SymbolicSquareMatrix
    \brief A class for n x n symbolic square matrices
*/
class SymbolicSquareMatrix {
    private:
        int n;
        std::vector<std::vector<std::unique_ptr<Element>>> elements;
    public:

        /**
            \brief Default constructor
        */
        SymbolicSquareMatrix();

        /**
            \brief Parametric constructor
            \param s The matrix given as a string
            \throw std::invalid_argument if the given string is not a proper square matrix 
        */        
        SymbolicSquareMatrix(const std::string& str_m);

        /**
            \brief Copy constructor
            \param m The SymbolicSquareMatrix to be copied
        */
        SymbolicSquareMatrix(const SymbolicSquareMatrix& m);

        /**
            \brief Move copy constructor
            \param m The SymbolicSquareMatrix to be moved
        */
        SymbolicSquareMatrix(SymbolicSquareMatrix&& m);

        /**
            \brief Default destructor
        */
        virtual ~SymbolicSquareMatrix()=default;

        /**
            \brief Overloaded copy assignment operator =
            \param m SymbolicSquareMatrix to be assigned
            \return The resulting SymbolicSquareMatrix as a reference
        */
        SymbolicSquareMatrix& operator = (const SymbolicSquareMatrix& m);

        /**
            \brief Overloaded move assignment operator =
            \param m SymbolicSquareMatrix to be assigned
            \return The resulting SymbolicSquareMatrix as a reference
        */
        SymbolicSquareMatrix& operator = (SymbolicSquareMatrix&& m);

        /**
            \brief Transpose function
            \return Transposed copy of the SymbolicSquareMatrix
        */
        SymbolicSquareMatrix transpose() const;

        /**
            \brief Overloaded comparison operator ==
            \param m SymbolicSquareMatrix to be compared
            \return Boolean value
        */
        bool operator == (const SymbolicSquareMatrix& m) const;

        /**
            \brief Print function for SymbolicSquareMatrix class, outputs to std::ostream
            \param os reference to an ostream where the representation is wanted
        */
        void print(std::ostream& os) const;

        /**
            \brief Function to transform the SymbolicSquareMatrix to a string
            \return The resulting string as a copy
        */
        std::string toString() const;

        /**
            \brief Function to transform the SymbolicSquareMatrix to a ConcreteSquareMatrix
            \param val Valuation map
            \return The resulting ConcreteSquareMatrix as a copy
            \throw std::out_of_range if a variable in the SymbolicSquareMatrix is not found in the valuation map 
        */
        ConcreteSquareMatrix evaluate(const Valuation& val) const;

        /**
            \brief Getter for param n
            \return value of n (int)
        */
        int getN() const;
        
        /**
            \brief Function to check if given matrix is a valid square matrix
            \param str_m A reference to std::string representation of a matrix
            \return A boolean value
        */
        bool isSquareMatrix(const std::string& str_m);
};

/**
    \brief Overloaded operator << (string insertion) for SymbolicSquareMatrix objects
    \param os std::ostream&
    \param sm const SymbolicSquareMatrix&
    \return The resulting SymbolicSquareMatrix as a copy
*/
std::ostream& operator<<(std::ostream& os, const SymbolicSquareMatrix& sm);
#endif
