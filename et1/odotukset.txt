Juho Holmi 2595403

Odotukset olio-ohjelmoinnin jatkokurssin suhteen

Odotukseni tätä kurssia kohtaan ovat korkeat. Alun perusteella kurssi tulee opettamaan erittäin tärkeitä ja työelämän kannalta hyödyllisiä taitoja, mm. gitin, ohjelmistotestaamisen, C++:n ja monta muuta asiaa. Viime keväänä kävin parissa työhaastattelussa, joissa tuli esille C++:n hyödyllisyys ainakin sulautettuihin järjestelmiin liittyvissä työpaikoissa. Gitiä olen käyttänyt jonkun verran aiemmin, mutta toivon myös, että tämä kurssi opettaa sen käytöstä uutta. Yksikkö- tai muunkaan laista testaamista en ole aiemmin tehnyt enkä myöskään koodin analysointia minkäänlaisilla työkaluilla, joten odotan oppivani niistä paljon uutta ja hyödyllistä asiaa.

Ainiin ja matriisit tulevat myös aivan uutena asiana.
