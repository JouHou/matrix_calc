/**
    \file varelement.hpp
    \brief Header for VarElement class
*/
#ifndef VARELEMENT_H_INCLUDED
#define VARELEMENT_H_INCLUDED
#include <ostream>
#include <memory>
#include "valuation.hpp"
#include "element.hpp"

/**
    \class VarElement
    \brief A class for single matrix elements
*/
class VariableElement : public Element {
    private:
        char val;
    public:

        /**
            \brief Default constructor
        */
        VariableElement();

        /**
            \brief Parametric constructor
            \param v Matrix element value
        */     
        VariableElement(char v);

        /**
            \brief Default destructor
        */
        virtual ~VariableElement()=default;

        /**
            \brief Getter for param val
            \return Matrix element value
        */
        char getVal() const;

        /**
            \brief Setter for param val
            \param v Matrix element value to be set
        */
        void setVal(char v);

        /**
            \brief Function to clone the element
            \return std::unique_ptr<IntElement> referring to the element
        */
        std::unique_ptr<Element> clone();   

        /**
            \brief Function to transform the IntElement to a string
            \return The resulting string
        */
        std::string toString() const;

        /**
            \brief Valuation function
            \return int
        */
        int evaluate(const Valuation& v) const;

        /**
            \brief Overloaded operator ==
            \param i IntElement to be compared
            \return Boolean value
        */
        bool operator == (const VariableElement& i) const;
};

#endif
