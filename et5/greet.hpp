#ifndef GREET_H_INCLUDED
#define GREET_H_INCLUDED
#include <vector>
#include <string>
#include <memory>

class Greet {
    public:
        virtual ~Greet()=default;
        virtual std::string greet() const = 0;
};

class HelloGreet : public Greet {
    public:
        std::string greet() const;
};

class MoroGreet : public Greet {
    public:
        std::string greet() const;
};

class Greeter {
    protected:
        std::vector<Greet*> greetings;
    public:
        Greeter(Greet* g);
        virtual ~Greeter();
        void addGreet(Greet* g);
        std::string sayHello() const;
};

#endif
