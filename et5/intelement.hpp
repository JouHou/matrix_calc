/**
    \file intelement.hpp
    \brief Header for IntElement class
*/
#ifndef INTELEMENT_H_INCLUDED
#define INTELEMENT_H_INCLUDED
#include <ostream>
#include <memory>
#include "varelement.hpp"
#include "element.hpp"
#include "valuation.hpp"

/**
    \class IntElement
    \brief A class for single matrix elements
*/
class IntElement : public Element {
    private:
        int val;
    public:

        /**
            \brief Default constructor
        */
        IntElement();

        /**
            \brief Parametric constructor
            \param v Matrix element value
        */     
        IntElement(int v);

        /**
            \brief Default destructor
        */
        virtual ~IntElement()=default;

        /**
            \brief Getter for param val
            \return Matrix element value
        */
        int getVal() const;

        /**
            \brief Setter for param val
            \param v Matrix element value to be set
        */
        void setVal(int v);

        /**
            \brief Function to clone the element
            \return std::unique_ptr<IntElement> referring to the element
        */
        std::unique_ptr<Element> clone() const;   

        /**
            \brief Function to transform the IntElement to a string
            \return The resulting string
        */
        std::string toString() const;

        /**
            \brief Valuation function
            \return int
        */
        int evaluate(const Valuation& v) const;

        /**
            \brief Overloaded operator +=
            \return The resulting IntElement as a reference
        */
        IntElement& operator += (const IntElement& i);

        /**
            \brief Overloaded operator -=
            \param i IntElement to be subtracted
            \return The resulting IntElement as a reference
        */
        IntElement& operator -= (const IntElement& i);

        /**
            \brief Overloaded operator *=
            \param i IntElement to be multiplied
            \return The resulting IntElement as a reference
        */
        IntElement& operator *= (const IntElement& i);

        /**
            \brief Overloaded operator ==
            \param i IntElement to be compared
            \return Boolean value
        */
        bool operator == (const IntElement& i) const;
};

/**
    \brief Overloaded operator + (addition) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator+(const IntElement& e1, const IntElement& e2);

/**
    \brief Overloaded operator - (subtraction) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator-(const IntElement&, const IntElement&);

/**
    \brief Overloaded operator * (multiplication) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator*(const IntElement&, const IntElement&);
#endif