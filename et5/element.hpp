#ifndef ELEMENT_H_INCLUDED
#define ELEMENT_H_INCLUDED
#include <string>
#include <memory>
#include "valuation.hpp"

class Element {
    public:
        virtual ~Element() = 0;
        virtual std::unique_ptr<Element> clone() const = 0;
        virtual std::string toString() const = 0;
        virtual int evaluate(const Valuation& val) const = 0;
};

/**
    \brief Overloaded operator << for IntElement object
    \param os Output stream
    \param e IntElement to be output to the stream
    \return Output stream
*/
std::ostream& operator<<(std::ostream& os, const Element& e);

#endif
