/**
    \file greeter_tests.cpp
    \brief Source code for greeter unit tests
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "greet.hpp"


TEST_CASE("Greeter tests","[correct]"){
    Greeter grtr(new HelloGreet);
    CHECK(grtr.sayHello() == "Hello!");
    grtr.addGreet(new MoroGreet);
    CHECK(grtr.sayHello() == "Hello!\nMoro!");
}
