/**
    \file greeter.cpp
    \brief Source code for greeter class
*/
#include <string>
#include <sstream>
#include <iostream>
#include <memory>
#include <vector>
#include "greet.hpp"

Greeter::Greeter(Greet* g):greetings{g} {}
Greeter::~Greeter() {
    for (auto g : greetings) {
        delete(g);
    }
}
void Greeter::addGreet(Greet* g) {
    greetings.push_back(g);
}
std::string Greeter::sayHello() const {
    std::stringstream strm;
    bool alku = true;
    for (const auto& grtng : greetings) {
        if (!alku) {
            strm << std::endl << grtng->greet();
        } else {
            strm << grtng->greet();
            alku = false;
        }
    }
    return strm.str();
}
std::string HelloGreet::greet() const {
    return "Hello!";
}
std::string MoroGreet::greet() const {
    return "Moro!";
}
/*
Greeter::Greeter(const std::string& g) {
    greetings = {new std::string(g)};
}

Greeter::Greeter(const Greeter& other) {
    for(auto grtng : other.greetings) {
        greetings.push_back(new std::string(*grtng));
    }
}

Greeter::Greeter(Greeter&& other) {
    greetings = std::move(other.greetings);
}

Greeter::~Greeter() {
    for(auto grtng : greetings) {
        delete(grtng);
    }
}

Greeter& Greeter::operator=(const Greeter& other) {
    if (this == &other) return *this;
    Greeter gcopy{other};
    greetings.swap(gcopy.greetings);
    return *this;
}

Greeter& Greeter::operator=(Greeter&& other) {
    if (this == &other) return *this;
    for(auto grtng : greetings) {
        delete(grtng);
    }    
    greetings = std::move(other.greetings);
    return *this;
}

void Greeter::addGreet(const std::string& g) {
    greetings.push_back(new std::string(g));
}

std::string Greeter::sayHello() const {
    std::stringstream strm;
    bool alku = true;
    for (const auto& grtng : greetings) {
        if (!alku) {
            strm << std::endl << *grtng;
        } else {
            strm << *grtng;
            alku = false;
        }
    }
    return strm.str();
}
*/