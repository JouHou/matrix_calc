/**
    \file element.cpp
    \brief Source code for Element class
*/
#include <ostream>
#include "element.hpp"

std::ostream& operator<<(std::ostream& os, const Element& e) {
    os << e.toString();
    return os;
}

bool operator==(const Element& e1, const Element& e2) {
    return e1.toString() == e2.toString();
}