/**
     \file compositeelement_tests.cpp
    \brief Unit tests for CompositeElement class
*/

#include "catch.hpp"
#include "compositeelement.hpp"
#include "element.hpp"
#include "intelement.hpp"
#include "varelement.hpp"
#include <functional>
#include <memory>

TEST_CASE("CompositeElement default constructor and toString() test", "[CompositeElement]") {
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::plus<int>(),'+'};
    CHECK(ce.toString() == "(2+x)");
}

TEST_CASE("CompositeElement copy constructor and toString() test", "[CompositeElement]") {
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::plus<int>(),'+'};
    CompositeElement ce2{ce};
    CHECK(ce2.toString() == "(2+x)");
}

TEST_CASE("CompositeElement operator = and toString() test", "[CompositeElement]") {
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::plus<int>(),'+'};
    CompositeElement ce2{ie, ve, std::minus<int>(),'-'};
    ce2 = ce;
    CHECK(ce2.toString() == "(2+x)");
}

TEST_CASE("CompositeElement clone() and toString() test", "[CompositeElement]") {
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::plus<int>(),'+'};
    CompositeElement* p = ce.clone();
    CHECK(p->toString() == "(2+x)");
    delete(p);
}


TEST_CASE("CompositeElement operator + test", "[CompositeElement]") {
    Valuation valu;
    valu['x'] = 2;
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::plus<int>(),'+'};
    CHECK(ce.evaluate(valu) == 4);
}

TEST_CASE("CompositeElement operator - test", "[CompositeElement]") {
    Valuation valu;
    valu['x'] = 2;
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::minus<int>(),'+'};
    CHECK(ce.evaluate(valu) == 0);
}
TEST_CASE("CompositeElement operator * test", "[CompositeElement]") {
    Valuation valu;
    valu['x'] = 2;
    IntElement ie{2};
    VariableElement ve{'x'};
    CompositeElement ce{ie, ve, std::multiplies<int>(),'+'};
    CHECK(ce.evaluate(valu) == 4);
}