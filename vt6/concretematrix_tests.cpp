/**
    \file concretematrix_tests.cpp
    \brief Unit tests for ConcreteSquareMatrix class
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "concretematrix.hpp"
#include "varelement.hpp"
#include "valuation.hpp"
#include "symbolicmatrix.hpp"

TEST_CASE("ConcreteSquareMatrix default constructor test", "[ConcreteSquareMatrix, correct]") {
    ConcreteSquareMatrix sm1;
    CHECK(sm1.getN() == 0);
    CHECK(sm1.toString() == "[]");
}

TEST_CASE("ConcreteSquareMatrix parametric constructor tests", "[ConcreteSquareMatrix, correct]") {
    CHECK_NOTHROW(ConcreteSquareMatrix("[]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[0]]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[2]]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[-2]]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[1,2][1,2]]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[1,2,3][1,2,3][1,2,3]]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[1,2,3,4][1,2,3,4][1,2,3,4][1,2,3,4]]"));
    CHECK_NOTHROW(ConcreteSquareMatrix("[[1,2,3,4,5][1,2,3,4,5][1,2,3,4,5][1,2,3,4,5][1,2,3,4,5]]"));
}

TEST_CASE("ConcreteSquareMatrix copy constructor test", "[ConcreteSquareMatrix, correct]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2(sm);
    CHECK(sm.toString() == sm2.toString());
}

TEST_CASE("ConcreteSquareMatrix move copy constructor test, Correct", "[ConcreteSquareMatrix, correct]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2(std::move(sm));
    CHECK(sm2.toString() == "[[2,2][2,2]]");
}

TEST_CASE("ConcreteSquareMatrix parametric constructor tests, Incorrect size", "[ConcreteSquareMatrix, incorrect]") {
    CHECK_THROWS_AS(ConcreteSquareMatrix("[[1,2,3][1,2]]"), std::invalid_argument);
    CHECK_THROWS_AS(ConcreteSquareMatrix("[[1,2,3][1,2,3]]"), std::invalid_argument);
}

TEST_CASE("ConcreteSquareMatrix parametric constructor tests, bracket problems", "[ConcreteSquareMatrix, incorrect]") {
    CHECK_THROWS(ConcreteSquareMatrix("[1,2]")); // missing inner brackets
    CHECK_THROWS(ConcreteSquareMatrix("[[1}}")); // non-matching brackets
    CHECK_THROWS(ConcreteSquareMatrix("[[1,2][1,2]")); // missing closing bracket
    CHECK_THROWS(ConcreteSquareMatrix("((1,2)(1,2))")); // wrong kind of brackets
}

TEST_CASE("ConcreteSquareMatrix parametric constructor tests, Malformed input", "[ConcreteSquareMatrix, incorrect]") {
    CHECK_THROWS(ConcreteSquareMatrix("asdf")); // pure garbage
    CHECK_THROWS(ConcreteSquareMatrix("[[|]]")); // pure garbage
    CHECK_THROWS(ConcreteSquareMatrix("[[1,][1,]]")); // missing numbers
    CHECK_THROWS(ConcreteSquareMatrix("[[1,,2][1,,2]]")); // extra commas
    CHECK_THROWS(ConcreteSquareMatrix("[[1a,2a][1a,2a]]")); // useless alphabets
    CHECK_THROWS(ConcreteSquareMatrix("[[1.0]]")); //  correct size, but float instead of int
    CHECK_THROWS(ConcreteSquareMatrix("[[2,2][2,2]]aa")); // correct matrix, but excess garbage in the tail

    CHECK_THROWS_AS(ConcreteSquareMatrix("[[2,2][2,2]]aa"), std::invalid_argument);
    CHECK_THROWS_WITH(ConcreteSquareMatrix("[[2,2][2,2]]aa"), "Not a square matrix!");
}

TEST_CASE("ConcreteSquareMatrix toString() test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    CHECK(sm.toString() == "[[2,2][2,2]]");
}

TEST_CASE("ConcreteSquareMatrix getN() test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    CHECK(sm.getN() == 2);
}

TEST_CASE("ConcreteSquareMatrix print() test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    std::stringstream strm;
    sm.print(strm);
    CHECK(strm.str() == "[[2,2][2,2]]");
}

TEST_CASE("ConcreteSquareMatrix operator << test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    std::stringstream strm;
    strm.str("");
    strm << sm;
    CHECK(strm.str() == "[[2,2][2,2]]");
}

TEST_CASE("ConcreteSquareMatrix copy assignment operator test", "[ConcreteSquareMatrix]") {

    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2("[[1,1][1,1]]");
    sm = sm2;
    CHECK(sm==sm2);
}

TEST_CASE("ConcreteSquareMatrix move assignment operator test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2("[[1,1][1,1]]");
    sm = std::move(sm2);
    CHECK(sm.toString() == "[[1,1][1,1]]");
}

TEST_CASE("ConcreteSquareMatrix operator += test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2(sm);
    CHECK((sm+=sm2).toString() == "[[4,4][4,4]]");
}

TEST_CASE("ConcreteSquareMatrix operator -= test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2("[[5,5][5,5]]");
    CHECK((sm2-=sm).toString() == "[[3,3][3,3]]");
}

TEST_CASE("ConcreteSquareMatrix operator *= test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[1,2,3][4,5,6][7,8,9]]");
    ConcreteSquareMatrix sm2("[[1,2,3][4,5,6][7,8,9]]");
    CHECK((sm*=sm2).toString() == "[[30,36,42][66,81,96][102,126,150]]");
}

TEST_CASE("ConcreteSquareMatrix compound assignment operator tests, incorrect sizes", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2("[[1,2,3][4,5,6][7,8,9]]");
    // Test operations with different-sized matrices - should throw exception //
    CHECK_THROWS_AS(sm-=sm2, std::domain_error);
    CHECK_THROWS_AS(sm+=sm2, std::domain_error);
    CHECK_THROWS_AS(sm*=sm2, std::domain_error);
}

TEST_CASE("ConcreteSquareMatrix transpose test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm1("[[1,2,3][4,5,6][7,8,9]]");
    CHECK(sm1.transpose().toString() == "[[1,4,7][2,5,8][3,6,9]]");
}

TEST_CASE("ConcreteSquareMatrix operator == test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2(sm);
    ConcreteSquareMatrix sm3("[[1,1,1][1,1,1][1,1,1]]");
    CHECK(sm2==sm);
    CHECK_FALSE(sm==sm3);
}

TEST_CASE("ConcreteSquareMatrix operator + test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,1,1][1,1,1][1,1,1]]");
    ConcreteSquareMatrix sm2("[[1,2,3][4,5,6][7,8,9]]");
    CHECK((sm + sm2).toString() == "[[3,3,4][5,6,7][8,9,10]]");
}

TEST_CASE("ConcreteSquareMatrix operator - test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,1,1][1,1,1][1,1,1]]");
    ConcreteSquareMatrix sm2("[[1,2,3][4,5,6][7,8,9]]");
    CHECK((sm2 - sm).toString() == "[[-1,1,2][3,4,5][6,7,8]]");
}

TEST_CASE("ConcreteSquareMatrix operator * test", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,1,1][1,1,1][1,1,1]]");
    ConcreteSquareMatrix sm2("[[1,2,3][4,5,6][7,8,9]]");
    CHECK((sm2 * sm2).toString() == "[[30,36,42][66,81,96][102,126,150]]");
}

TEST_CASE("ConcreteSquareMatrix binary operator tests, unmatching operands", "[ConcreteSquareMatrix]") {
    ConcreteSquareMatrix sm("[[2,2][2,2]]");
    ConcreteSquareMatrix sm2("[[2,1,1][1,1,1][1,1,1]]");

    CHECK_THROWS_AS(sm2+sm, std::domain_error);
    CHECK_THROWS_AS(sm2-sm, std::domain_error);
    CHECK_THROWS_AS(sm2*sm, std::domain_error);
}