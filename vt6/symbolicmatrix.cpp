/**
    \file squarematrix.cpp
    \brief Source code for SymbolicSquareMatrix class
*/
#include "symbolicmatrix.hpp"
#include "concretematrix.hpp"
#include "intelement.hpp"
#include "compositeelement.hpp"
#include <sstream>
#include <algorithm>
#include <stdexcept>
#include <memory>

SymbolicSquareMatrix::SymbolicSquareMatrix() :elements {} { // default constructor, initialize to n=0 and empty vector
    n = 0;
}

SymbolicSquareMatrix::SymbolicSquareMatrix(const std::string& str_m) { // parametric constructor
    if(!isSquareMatrix(str_m)) // returns false if str_m is not in a correct form
        throw std::invalid_argument("Not a square matrix!");
}

SymbolicSquareMatrix::SymbolicSquareMatrix(const SymbolicSquareMatrix& m) { // copy constructor
    for(auto& mrow : m.elements) {
        elements.push_back({}); // push back an empty row
        for(auto& melem : mrow) {
            elements.back().push_back(std::unique_ptr<Element>(melem->clone())); // push back a clone of the element
        }
    }
    n = m.n;
}

SymbolicSquareMatrix::SymbolicSquareMatrix(SymbolicSquareMatrix&& m) : elements(std::move(m.elements)) { // move copy constructor
    n = m.n;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator = (const SymbolicSquareMatrix& m) { // copy assignment operator
    if (this == &m) return *this; // if trying to assign onto itself

    SymbolicSquareMatrix tmpMtrx{m};
    std::swap(elements, tmpMtrx.elements);
    n = m.n;
    return *this;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator = (SymbolicSquareMatrix&& m) { // move assignment operator
    if (this == &m) return *this; // if trying to assign onto itself

    elements = std::move(m.elements);
    n = m.n;
    return *this;
}

SymbolicSquareMatrix SymbolicSquareMatrix::transpose() const { // return a transposed copy of the matrix
    SymbolicSquareMatrix tmpMtrx;
    tmpMtrx.n = n;

    for (int i=0;i<n;i++) { // row
        tmpMtrx.elements.push_back({});
        for (int j=0;j<n;j++) { // column
            tmpMtrx.elements.at(i).push_back(std::unique_ptr<Element>(elements.at(j).at(i)->clone()));
        }
    }
    return tmpMtrx;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator+(const SymbolicSquareMatrix& m) const {
    if (n != m.n)
        throw std::domain_error("Matrices must be of the same size!");
    SymbolicSquareMatrix tmpSmMtrx;
    tmpSmMtrx.n = n;

    for (int i = 0; i<n; i++) {
        tmpSmMtrx.elements.push_back({}); // Add row
        for (int j = 0; j<n; j++) { // Add the element to i,j
            tmpSmMtrx.elements.back().push_back(std::unique_ptr<Element>(new CompositeElement{*elements.at(i).at(j),*m.elements.at(i).at(j),std::plus<int>(),'+'}));
        }
    }
    return tmpSmMtrx;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator-(const SymbolicSquareMatrix& m) const {
    if (n != m.n)
        throw std::domain_error("Matrices must be of the same size!");
    SymbolicSquareMatrix tmpSmMtrx; // Initialize an empty SymbolicSquareMatrix
    tmpSmMtrx.n = n;

    for (int i = 0; i<n; i++) {
        tmpSmMtrx.elements.push_back({}); // Add row
        for (int j = 0; j<n; j++) { // Add the element to i,j
            tmpSmMtrx.elements.back().push_back(std::unique_ptr<Element>(new CompositeElement{*elements.at(i).at(j),*m.elements.at(i).at(j),std::minus<int>(),'-'}));
        }
    }
    return tmpSmMtrx;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator*(const SymbolicSquareMatrix& m) const {
    if (n != m.n) {
        throw std::domain_error("Matrices must be of the same size!");
    }
    SymbolicSquareMatrix tmpSmMtrx; // Initialize an empty SymbolicSquareMatrix
    tmpSmMtrx.n = n;
    for (int i=0;i<n;i++) { // row
        tmpSmMtrx.elements.push_back({}); // Add row
        for (int j=0;j<n;j++) {  // column
            tmpSmMtrx.elements.back().push_back(std::unique_ptr<Element>(new IntElement{0})); // Add the first element to i,j
            for (int k = 0;k<n;k++) {
                auto temp = std::unique_ptr<Element>(new CompositeElement{*elements.at(i).at(k),*m.elements.at(k).at(j),std::multiplies<int>(),'*'});
                tmpSmMtrx.elements.at(i).at(j) = std::unique_ptr<Element>(new CompositeElement{*tmpSmMtrx.elements.at(i).at(j),*temp,std::plus<int>(),'+'});
            }
        }
    }
    return tmpSmMtrx;
}

bool SymbolicSquareMatrix::operator == (const SymbolicSquareMatrix& m) const { // comparison operator
    if (n != m.n) {
        return false; // if dimensions don't match, clearly not equal
    }
    return (toString() == m.toString()); // equal matrices should transform into equal strings
}

void SymbolicSquareMatrix::print(std::ostream& os) const {
    os << toString();
}

std::string SymbolicSquareMatrix::toString() const { // return a string representation of the matrix
    std::stringstream strm;
    strm << "[";
    for (const auto& row : elements) { // row
        strm << "[";
        bool first = true;
        for (const auto& elem : row) { // element in the row
            if (!first) { // don't put a comma before the first number
                 strm << ",";
            } 
            strm << *elem;
            first = false;
        }
        strm << "]";
    }
    strm << "]";

    return strm.str();
}

ConcreteSquareMatrix SymbolicSquareMatrix::evaluate(const Valuation& val) const { // create a ConcreteSquareMatrix from SymbolicSquareMatrix
    ConcreteSquareMatrix csm;  // Initialize an empty ConcreteSquareMatrix
    csm.n = n;

    for(auto& row : elements) {
        csm.elements.push_back({}); // Add row
        for(auto& elem : row) {
            csm.elements.back().push_back(std::unique_ptr<IntElement>(new IntElement(elem->evaluate(val)))); // Add element
        }
    }
    return csm;
}

int SymbolicSquareMatrix::getN() const { // getter for param n
    return n;
}

bool SymbolicSquareMatrix::isSquareMatrix(const std::string& str) { // check if the given string is proper square matrix
    std::stringstream input(str);
    char c;
    int number;
    int rows = 0;
    int columns = 0;
    int columns_temp = 0;

    input >> c;
    
    if(!input.good() || c!='[') { // if the string is garbage in the beginning
        return false;
    }

    input >> c;

    while(input.good() && c != ']') { // stay in the loop while stream is good and no closing bracket is detected
        if(input.good() && c == '[') { // starting bracket of the row
            rows++;
            elements.push_back({});
            do { // start looking for numbers
                char symbol;
                symbol = input.peek();
                if (!input.good()) {
                    return false;
                }
                if(isalpha(symbol)) {
                    input >> c;
                    elements.back().push_back(std::move(std::unique_ptr<Element>(new VariableElement(c))));
                } else {
                    input >> number;
                    if (input.fail()) { // if there is no number right after starting bracket
                        return false;
                    }
                    elements.back().push_back(std::move(std::unique_ptr<IntElement>(new IntElement(number))));
                }

                columns_temp++;
                input >> c;
                if (c != ',' && c != ']') { // if, after the number, the char isn't "," or "]", the input string is malformed
                    return false;
                }
            } while (c != ']'); // stay in the loop while closing bracket of the row is not detected

            if (columns == 0) { // assign the first row's column number as baseline
                columns = columns_temp;
            } else {
                if (columns_temp != columns) { // if any row differs from the first one in number of columns, the matrix isn't square matrix
                    return false;                
                }
            }

            columns_temp = 0;
        } else { // we haven't detected proper starting bracket of the row
            return false;
        }
        input >> c;
        if (input.eof()) { // if end of stream here, we haven't detected closign bracket, so the input string is malformed
            return false;
        }
    }

    std::string tail;
    input >> tail;
    if (tail!="") { // garbage after proper matrix
        return false;
    }

    if (rows == columns) { // if number of rows and columns match
        n = rows;
        return true;
    } else { // if number of rows and columns don't match, no square matrix
        return false;
    }
}

std::ostream& operator<<(std::ostream& os, const SymbolicSquareMatrix& sm) {
    os << sm.toString();
    return os;
}
