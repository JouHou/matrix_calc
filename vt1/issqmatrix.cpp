#include <iostream>
#include <sstream>
#include <string>
#include "issqmatrix.hpp"

bool isSquareMatrix(const std::string& str) {
    std::stringstream input(str);
    char c;
    int number;
    std::string tail;
    int rivit = 0;
    int sarakkeet = 0;
    int sarakkeet_temp = 0;
    int j = 0;

    if (str == "[]") {
        return true;
    }

    input >> c;
    
    if(!input.good() || c!='[') {
        return false;
    }

    input >> c;

    while(input.good() && c != ']') {
        if(input.good() && c == '[') {
            rivit++;
            do {
                input >> number;
                if (input.fail()) {
                    return false;
                }
                sarakkeet_temp++;
                input >> c;
                if (c != ',' && c != ']') {
                    return false;
                }
            } while (c != ']');

            if (j == 0) {
                sarakkeet = sarakkeet_temp;
            } else {
                if (sarakkeet_temp != sarakkeet) {
                    return false;                
                }
            }

            j++;
            sarakkeet_temp = 0;
        } else {
            return false;
        }
        input >> c;
        if (input.eof()) {
            return false;
        }
    }

    input >> tail;
    if (tail!="") {
        return false;
    }

    if (rivit == sarakkeet) {
        return true;
    } else {
        return false;
    }
}
