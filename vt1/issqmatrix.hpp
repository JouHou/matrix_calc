#ifndef ISSQMATRIX_H_INCLUDED
#define ISSQMATRIX_H_INCLUDED
#include <iostream>
#include <sstream>

bool isSquareMatrix(const std::string&);
#endif
