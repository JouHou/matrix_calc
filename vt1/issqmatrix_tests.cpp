#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include "issqmatrix.hpp"

TEST_CASE("Square matrix test one","[correct]"){
    CHECK(isSquareMatrix("[[1,2,3][1,2,3][1,2,3]]"));
    CHECK(isSquareMatrix("[]"));
    CHECK(isSquareMatrix("[[1,2][1,2]]"));
    CHECK(isSquareMatrix("[[2]]"));
    CHECK(isSquareMatrix("[[-2]]"));
    CHECK(isSquareMatrix("[[0]]"));
    CHECK(isSquareMatrix("[[1,2,3,4][1,2,3,4][1,2,3,4][1,2,3,4]]"));
}

TEST_CASE("Square matrix test two","[incorrect]"){
    CHECK_FALSE(isSquareMatrix("[1,2]"));
    CHECK_FALSE(isSquareMatrix("[1,]"));
    CHECK_FALSE(isSquareMatrix("[a1,2]"));
    CHECK_FALSE(isSquareMatrix("[[|]]"));
    CHECK_FALSE(isSquareMatrix("[a1,a2]"));
    CHECK_FALSE(isSquareMatrix("[1a,2a]"));
    CHECK_FALSE(isSquareMatrix("[1,2]aa151gg13"));
    CHECK_FALSE(isSquareMatrix("[[1]]aa151gg13"));
    CHECK_FALSE(isSquareMatrix("[1,2]"));
    CHECK_FALSE(isSquareMatrix("[1,,,,,,2]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3][1,2,3]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3][1,2]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2][1,2]"));
    CHECK_FALSE(isSquareMatrix("((1,2)(1,2))"));
    CHECK_FALSE(isSquareMatrix("asdf"));
    CHECK_FALSE(isSquareMatrix("[[1,2[1,2]"));
    CHECK_FALSE(isSquareMatrix(" [[1,2][1,2]"));
}
