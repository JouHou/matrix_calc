#ifndef UNIQPTR_OUTPUT_H_INCLUDED
#define UNIQPTR_OUTPUT_H_INCLUDED

template <typename T> std::ostream& operator << (std::ostream& os, const std::unique_ptr<T>& tptr) {
    os << *tptr;
    return os;
}

#endif
