#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "uniq_ptr_output.hpp"
#include <memory>
#include <string>
#include <iostream>

TEST_CASE("Template class operator << test", "[Template <<]" ) {
    std::stringstream strm;

    std::unique_ptr<int> kaksiuniqptr = std::unique_ptr<int>(new int(2)); // test integer
    strm << kaksiuniqptr;
    CHECK(strm.str() == "2");

    strm.str(""); // empty the stream

    std::unique_ptr<std::string> stringuniqptr(new std::string("Test")); // test std::string
    strm << stringuniqptr;
    CHECK(strm.str() == "Test");

    strm.str(""); // empty the stream

    std::unique_ptr<double> doubleuniqptr(new double(1.55)); // test double
    strm << doubleuniqptr;
    CHECK(strm.str() == "1.55");

    strm.str(""); // empty the stream

    std::unique_ptr<bool> booluniqptr(new bool(true)); // test bool
    strm << booluniqptr;
    CHECK(strm.str() == "1");

    strm.str(""); // empty the stream

    std::unique_ptr<char> charuniqptr(new char('t')); // test char
    strm << charuniqptr;
    CHECK(strm.str() == "t");
}