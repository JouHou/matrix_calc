/**
    \file greeter_tests.cpp
    \brief Source code for greeter unit tests
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "greeter.hpp"


TEST_CASE("Subject xxx test one","[correct]"){
    Greeter grtr{"jou"};
    CHECK(grtr.sayHello() == "jou");
    grtr.addGreet("man");
    CHECK(grtr.sayHello() == "jou\nman");
}