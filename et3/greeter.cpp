/**
    \file greeter.cpp
    \brief Source code for greeter class
*/
#include <string>
#include <sstream>
#include "greeter.hpp"

Greeter::Greeter(const std::string& g) {
    greetings = {g};
}

void Greeter::addGreet(const std::string& g) {
    greetings.push_back(g);
}

std::string Greeter::sayHello() const {
    std::stringstream strm;
    bool alku = true;
    for (auto grtng : greetings) {
        if (!alku) {
            strm << std::endl << grtng;
        } else {
            strm << grtng;
            alku = false;
        }
    }
    return strm.str();
}
