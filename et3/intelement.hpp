/**
    \file intelement.hpp
    \brief Header for IntElement class
*/
#ifndef INTELEMENT_H_INCLUDED
#define INTELEMENT_H_INCLUDED
#include <ostream>

/**
    \class IntElement
    \brief A class for single matrix elements
*/
class IntElement {
    private:
        int val;
    public:
        /**
            \brief Default constructor
        */
        IntElement();
        /**
            \brief Parametric constructor
            \param v Matrix element value
        */     
        IntElement(int v);
        /**
            \brief Default destructor
        */
        virtual ~IntElement()=default;
        /**
            \brief Getter for param val
            \return Matrix element value
        */
        int getVal() const;
        /**
            \brief Setter for param val
            \param v Matrix element value to be set
        */
        void setVal(int v);
        /**
            \brief Overloaded operator +=
            \param i IntElement to be added
            \return The resulting IntElement
        */
        IntElement& operator += (const IntElement& i);
        /**
            \brief Overloaded operator -=
            \param i IntElement to be subtracted
            \return The resulting IntElement
        */
        IntElement& operator -= (const IntElement& i);
        /**
            \brief Overloaded operator *=
            \param i IntElement to be multiplied
            \return The resulting IntElement
        */
        IntElement& operator *= (const IntElement& i);
        /**
            \brief Overloaded operator ==
            \param i IntElement to be compared
            \return Boolean value
        */
        bool operator == (const IntElement& i) const;
        /**
            \brief Function to transform the IntElement to a string
            \return The resulting string
        */
        std::string toString() const;
};
/**
    \brief Overloaded operator + (addition) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator+(const IntElement& e1, const IntElement& e2);
/**
    \brief Overloaded operator - (subtraction) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator-(const IntElement&, const IntElement&);
/**
    \brief Overloaded operator * (multiplication) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator*(const IntElement&, const IntElement&);
/**
    \brief Overloaded operator << for two IntElement objects
    \param os Output stream
    \param e IntElement to be output to the stream
    \return Output stream
*/
std::ostream& operator<<(std::ostream& os, const IntElement& e);
#endif