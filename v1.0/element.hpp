/**
    \file element.hpp
    \brief Header for Element interface class, IntElement class and VariableElement class
*/
#ifndef ELEMENT_H_INCLUDED
#define ELEMENT_H_INCLUDED
#include <string>
#include <memory>
#include <sstream> 
#include "valuation.hpp"

/**
    \class Element
    \brief An interface class for single matrix elements
*/
class Element {
    public:

        /**
            \brief Default destructor
        */
        virtual ~Element(){};

        /**
            \brief Clone function for elements
            \return Pointer to the cloned element
        */
        virtual Element* clone() const = 0;

        /**
            \brief toString function for elements
            \return Copy of resulting string
        */
        virtual std::string toString() const = 0;

        /**
            \brief Evaluation function for elements
            \param val Valuation map
            \return Int
            \throw std::out_of_range if called on VariableElement which doesn't have a matching variable
        */
        virtual int evaluate(const Valuation& val) const = 0;
};

/**
    \brief Overloaded operator << for IntElement object
    \param os Output stream
    \param e IntElement to be output to the stream
    \return Output stream
*/
std::ostream& operator<<(std::ostream& os, const Element& e);

/**
    \brief Overloaded comparison operator == for Element objects
    \param e1 First operand
    \param e2 Second operand
    \return Boolean value 
 */
bool operator==(const Element& e1, const Element& e2);

/**
 * \brief Template class for IntElements and VariableElements
 * \tparam Type Either int or char
 */
template<typename Type> class TElement : public Element {
    private:
        Type val;
    public:
        TElement():val(0){};
        TElement(Type t):val(t){};
        virtual ~TElement() = default;
        void setVal(Type t) { val = t;}
        Type getVal() const {return val;}
        virtual Element* clone() const override {
            return new TElement<Type>(*this);
        };
        virtual std::string toString() const override {
            std::stringstream strm;
            strm << val;
            return strm.str();
        };
        virtual int evaluate(const Valuation& val) const override;

        TElement<Type>& operator+=(const TElement<Type>& t);
        TElement<Type>& operator-=(const TElement<Type>& t);
        TElement<Type>& operator*=(const TElement<Type>& t);
};

using IntElement = TElement<int>;
using VariableElement = TElement<char>;

/**
    \brief Overloaded operator + (addition) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator+(const IntElement& e1, const IntElement& e2);

/**
    \brief Overloaded operator - (subtraction) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator-(const IntElement& e1, const IntElement& e2);

/**
    \brief Overloaded operator * (multiplication) for two IntElement objects
    \param e1 Left hand side operand
    \param e2 Right hand side operand
    \return The resulting IntElement
*/
IntElement operator*(const IntElement& e1, const IntElement& e2);
#endif
