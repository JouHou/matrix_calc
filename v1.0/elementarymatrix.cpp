/**
    \file elementarymatrix.cpp
    \brief Source code for ElementarySquareMatrix template class, SymbolicSquarematrix class and ConcreteSquareMatrix class
 */
#include <sstream>
#include <string>
#include "element.hpp"
#include "elementarymatrix.hpp"
#include "compositeelement.hpp"

template<> ConcreteSquareMatrix::ElementarySquareMatrix(const std::string& str_m) { // check if the given string is proper square matrix
    std::stringstream input(str_m);
    char c;

    input >> c;
    
    if(!input.good() || c!='[') { // if the string is garbage in the beginning
        throw std::invalid_argument("Not a square matrix!");
    }

    input >> c;

    int number;
    int rows = 0;
    int columns = 0;
    int columns_temp = 0;

    while(input.good() && c != ']') { // stay in the loop while stream is good and no closing bracket is detected
        if(input.good() && c == '[') { // starting bracket of the row
            rows++;
            elements.push_back({});
            do { // start looking for numbers
                input >> number;
                if (input.fail()) { // if there is no number right after starting bracket
                    throw std::invalid_argument("Not a square matrix!");
                }
                elements.back().push_back(std::move(std::unique_ptr<IntElement>(new IntElement(number))));
                columns_temp++;
                input >> c;
                if (c != ',' && c != ']') { // if, after the number, the char isn't "," or "]", the input string is malformed
                    throw std::invalid_argument("Not a square matrix!");
                }
            } while (c != ']'); // stay in the loop while closing bracket of the row is not detected

            if (columns == 0) { // assign the first row's column number as baseline
                columns = columns_temp;
            } else {
                if (columns_temp != columns) { // if any row differs from the first one in number of columns, the matrix isn't square matrix
                    throw std::invalid_argument("Not a square matrix!");                
                }
            }

            columns_temp = 0;
        } else { // we haven't detected proper starting bracket of the row
            throw std::invalid_argument("Not a square matrix!");
        }
        input >> c;
        if (input.eof()) { // if end of stream here, we haven't detected closign bracket, so the input string is malformed
            throw std::invalid_argument("Not a square matrix!");
        }
    }

    std::string tail;
    input >> tail;
    if (tail!="") { // garbage after proper matrix
        throw std::invalid_argument("Not a square matrix!");
    }

    if (rows == columns) { // if number of rows and columns match
        n = rows;
    } else { // if number of rows and columns don't match, no square matrix
        throw std::invalid_argument("Not a square matrix!");
    }
}

template<> SymbolicSquareMatrix::ElementarySquareMatrix(const std::string& str_m) { // check if the given string is proper square matrix
    std::stringstream input(str_m);
    char c;
    int number;
    int rows = 0;
    int columns = 0;
    int columns_temp = 0;

    input >> c;
    
    if(!input.good() || c!='[') { // if the string is garbage in the beginning
        throw std::invalid_argument("Not a square matrix!");
    }

    input >> c;

    while(input.good() && c != ']') { // stay in the loop while stream is good and no closing bracket is detected
        if(input.good() && c == '[') { // starting bracket of the row
            rows++;
            elements.push_back({});
            do { // start looking for numbers
                char symbol;
                symbol = input.peek();
                if (!input.good()) {
                    throw std::invalid_argument("Not a square matrix!");
                }
                if(isalpha(symbol)) {
                    input >> c;
                    elements.back().push_back(std::move(std::unique_ptr<Element>(new VariableElement(c))));
                } else {
                    input >> number;
                    if (input.fail()) { // if there is no number right after starting bracket
                        throw std::invalid_argument("Not a square matrix!");
                    }
                    elements.back().push_back(std::move(std::unique_ptr<IntElement>(new IntElement(number))));
                }

                columns_temp++;
                input >> c;
                if (c != ',' && c != ']') { // if, after the number, the char isn't "," or "]", the input string is malformed
                    throw std::invalid_argument("Not a square matrix!");
                }
            } while (c != ']'); // stay in the loop while closing bracket of the row is not detected

            if (columns == 0) { // assign the first row's column number as baseline
                columns = columns_temp;
            } else {
                if (columns_temp != columns) { // if any row differs from the first one in number of columns, the matrix isn't square matrix
                    throw std::invalid_argument("Not a square matrix!");                
                }
            }

            columns_temp = 0;
        } else { // we haven't detected proper starting bracket of the row
            throw std::invalid_argument("Not a square matrix!");
        }
        input >> c;
        if (input.eof()) { // if end of stream here, we haven't detected closign bracket, so the input string is malformed
            throw std::invalid_argument("Not a square matrix!");
        }
    }

    std::string tail;
    input >> tail;
    if (tail!="") { // garbage after proper matrix
        throw std::invalid_argument("Not a square matrix!");
    }

    if (rows == columns) { // if number of rows and columns match
        n = rows;
    } else { // if number of rows and columns don't match, no square matrix
        throw std::invalid_argument("Not a square matrix!");
    }
}

template<> ConcreteSquareMatrix& ConcreteSquareMatrix::operator += (const ConcreteSquareMatrix& m) {
    if (n != m.n) { // check that dimensions are same
        throw std::domain_error("Matrices must be of the same size!");
    } else {
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                *elements[i][j] += *m.elements[i][j]; // assign to this[i][j]
            }
        }
    }
    return *this;
}

template<> ConcreteSquareMatrix& ConcreteSquareMatrix::operator -= (const ConcreteSquareMatrix& m) {
    if (n != m.n) { // check that dimensions are same
        throw std::domain_error("Matrices must be of the same size!");
    } else {

        for(int i = 0; i < n; i++) { // row
            for(int j = 0; j < n; j++) { // column
                *elements[i][j] -= *m.elements[i][j]; // assign to this[i][j]
            }
        }
    }
    return *this;
}

template<> ConcreteSquareMatrix& ConcreteSquareMatrix::operator *= (const ConcreteSquareMatrix& m) {
    ConcreteSquareMatrix tmpMtrx(*this);
    if (n != m.n) {
        throw std::domain_error("Matrices must be of the same size!");
    } else {
        for (int i=0;i<n;i++) { // row
            for (int j=0;j<n;j++) {  // column
                for (int k = 0;k<n;k++) {
                    *elements[i][j] += *tmpMtrx.elements[i][k] * *m.elements[k][j]; // i,j = i,j + i,k * k,j  
                }
            }
        }
    }
    return *this-=tmpMtrx; // subtract the original matrix from the result here so no need for zero matrix
}

template<> SymbolicSquareMatrix SymbolicSquareMatrix::operator+(const SymbolicSquareMatrix& m) const {
    if (n != m.n)
        throw std::domain_error("Matrices must be of the same size!");
    SymbolicSquareMatrix tmpSmMtrx;
    tmpSmMtrx.n = n;

    for (int i = 0; i<n; i++) {
        tmpSmMtrx.elements.push_back({}); // Add row
        for (int j = 0; j<n; j++) { // Add the element to i,j
            tmpSmMtrx.elements.back().push_back(std::unique_ptr<Element>(new CompositeElement{*elements.at(i).at(j),*m.elements.at(i).at(j),std::plus<int>(),'+'}));
        }
    }
    return tmpSmMtrx;    
}

template<> SymbolicSquareMatrix SymbolicSquareMatrix::operator-(const SymbolicSquareMatrix& m) const {
    if (n != m.n)
        throw std::domain_error("Matrices must be of the same size!");
    SymbolicSquareMatrix tmpSmMtrx; // Initialize an empty SymbolicSquareMatrix
    tmpSmMtrx.n = n;

    for (int i = 0; i<n; i++) {
        tmpSmMtrx.elements.push_back({}); // Add row
        for (int j = 0; j<n; j++) { // Add the element to i,j
            tmpSmMtrx.elements.back().push_back(std::unique_ptr<Element>(new CompositeElement{*elements.at(i).at(j),*m.elements.at(i).at(j),std::minus<int>(),'-'}));
        }
    }
    return tmpSmMtrx;
}

template<> SymbolicSquareMatrix SymbolicSquareMatrix::operator*(const SymbolicSquareMatrix& m) const {
    if (n != m.n) {
        throw std::domain_error("Matrices must be of the same size!");
    }
    SymbolicSquareMatrix tmpSmMtrx; // Initialize an empty SymbolicSquareMatrix
    tmpSmMtrx.n = n;
    for (int i=0;i<n;i++) { // row
        tmpSmMtrx.elements.push_back({}); // Add row
        for (int j=0;j<n;j++) {  // column
            tmpSmMtrx.elements.back().push_back(std::unique_ptr<Element>(new IntElement{0})); // Add the first element to i,j
            for (int k = 0;k<n;k++) {
                auto temp = std::unique_ptr<Element>(new CompositeElement{*elements.at(i).at(k),*m.elements.at(k).at(j),std::multiplies<int>(),'*'});
                tmpSmMtrx.elements.at(i).at(j) = std::unique_ptr<Element>(new CompositeElement{*tmpSmMtrx.elements.at(i).at(j),*temp,std::plus<int>(),'+'});
            }
        }
    }
    return tmpSmMtrx;
}

template<> ConcreteSquareMatrix ConcreteSquareMatrix::operator+(const ConcreteSquareMatrix& m) const {
    if (n != m.n) { // if matrices are not same-sized, throw exception
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    ConcreteSquareMatrix tmpMtrx(*this);
    tmpMtrx+=m;
    return tmpMtrx;
}

template<> ConcreteSquareMatrix ConcreteSquareMatrix::operator-(const ConcreteSquareMatrix& m) const {
    if (n != m.n) { // if matrices are not same-sized, throw exception
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    ConcreteSquareMatrix tmpMtrx(*this);
    tmpMtrx-=m;
    return tmpMtrx;
}

template<> ConcreteSquareMatrix ConcreteSquareMatrix::operator*(const ConcreteSquareMatrix& m) const {
    if (n != m.n) { // if matrices are not same-sized, throw exception
        throw std::domain_error("Invalid operands - matrices must be of the same size!");
    }
    ConcreteSquareMatrix tmpMtrx(*this);
    tmpMtrx*=m;
    return tmpMtrx;
}
