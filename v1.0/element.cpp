/**
    \file element.cpp
    \brief Source code for Element class, IntElement class and VariableElement class
*/
#include <ostream>
#include "element.hpp"

std::ostream& operator<<(std::ostream& os, const Element& e) {
    os << e.toString();
    return os;
}

bool operator==(const Element& e1, const Element& e2) {
    return e1.toString() == e2.toString();
}

/***** VariableElement *****/

template<> int VariableElement::evaluate(const Valuation& v) const{
    return v.at(val);
}

/***** IntElement *****/

template<> IntElement& IntElement::operator+=(const IntElement& i) {
    val += i.val;
    return *this;

}

template<> IntElement& IntElement::operator-=(const IntElement& i) {
    val -= i.val;
    return *this;
}

template<> IntElement& IntElement::operator*=(const IntElement& i) {
    val *= i.val;
    return *this;
}

template<> int IntElement::evaluate(const Valuation& v) const{
    return val;
}

IntElement operator+(const IntElement& e1, const IntElement& e2) {
    IntElement elem(e1);
    elem += e2;
    return elem;
}

IntElement operator-(const IntElement& e1, const IntElement& e2) {
    IntElement elem(e1);
    elem -= e2;
    return elem;
}

IntElement operator*(const IntElement& e1, const IntElement& e2) {
    IntElement elem(e1);
    elem *= e2;
    return elem;
}