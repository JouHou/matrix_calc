/**
 * @file main.cpp
 * @brief A terminal UI for matrix calculator
 */

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "elementarymatrix.hpp"
#include <stack>
#include <string>
#include <iostream>
#include <regex>

int main(int argc, char** argv) {
    int result = Catch::Session().run( argc, argv );

    std::cout << "*** Welcome to matrix calculator! ***" << std::endl << std::endl;
    std::cout << "** Type matrices in form [[x,x][x,x]]. Remember, only square matrices!" << std::endl;
    std::cout << "** Type '+','-' or '*' to perform arithmetic operation on two topmost matrices." << std::endl;
    std::cout << "** Operands will be removed from stack and resulting matrix gets pushed to top of the stack." << std::endl;
    std::cout << "** Type '=' to evaluate the topmost matrix." << std::endl;
    std::cout << "** Type 'stacksize' to see how many matrices exist in the stack." << std::endl;
    std::cout << "** Type 'valuation' to see what variables exist in the valuation map." << std::endl;
    std::cout << "** Type 'quit' to exit the program." << std::endl << std::endl;

    std::string inputstring = "";
    std::stack<SymbolicSquareMatrix> matrixstack;
    Valuation valu;
    
    while (inputstring != "quit") {
        std::cout << "Type a command or a matrix: ";
        std::cin >> inputstring;
        std::regex valuregex("[[:alpha:]]=(\\+|-)?[[:digit:]]+", std::regex_constants::ECMAScript | std::regex_constants::icase); // capture valuation inputs with regex

        if (inputstring == "+" || inputstring == "-" || inputstring == "*") {
            if (matrixstack.size() < 2) {
                std::cout << "Not enough matrices" << std::endl;
            } else {
                SymbolicSquareMatrix sm = matrixstack.top();
                matrixstack.pop();
                SymbolicSquareMatrix sm2 = matrixstack.top();
                matrixstack.pop();

                try {
                    if (inputstring == "+") {
                        matrixstack.push(sm + sm2);
                    } else if (inputstring == "-") {
                        matrixstack.push(sm - sm2);                    
                    } else if (inputstring == "*") {
                        matrixstack.push(sm * sm2);
                    }
                    std::cout << matrixstack.top().toString() << std::endl; // print result of the operation
                } catch(const std::exception& e) { // usually this happens if the matrices aren't of same size
                    std::cerr << e.what();
                    while(matrixstack.size() != 0) { // empty the matrix stack
                        matrixstack.pop();
                    }
                    std::cout << " Matrix stack cleared, try again." << std::endl;
                }
                

            }

        } else if (inputstring == "=") {
            if (matrixstack.size() == 0) {
                std::cout << "Stack empty, type at least one matrix." << std::endl;
            } else {
                try {
                    ConcreteSquareMatrix cm = matrixstack.top().evaluate(valu);
                    std::cout << cm.toString() << std::endl << std::endl;
                } catch(const std::exception& e)  { // usually this happens if there aren't any variables in the valuation map
                    std::cerr << e.what() << '\n' << std::endl << std::endl;
                }
            }

        } else if (inputstring.rfind("[[", 0) == 0) { // if input string begins with "[[", should be a matrix 
            try
            {
                SymbolicSquareMatrix sm(inputstring);
                matrixstack.push(sm);
            } catch(const std::exception& e) { // usually this happens if the string doesn't represent a square matrix
                std::cerr << e.what() << '\n' << std::endl << std::endl;
            }
            
        } else if (std::regex_search(inputstring, valuregex)){ // if input string is in form x=5, add x equals 5 to valuation map
            char c = inputstring.at(0);
            std::string numberstring = inputstring.substr(2,inputstring.size());
            int num = stoi(numberstring);
            valu[c] = num;

        } else if (inputstring == "stacksize") { // print matrix stack size
            std::cout << "Matrix stack size: " << matrixstack.size() << std::endl << std::endl;

        } else if (inputstring == "valuation") { // print what variables exist in valuation map
            std::cout << "Variables in the valuation map: " << matrixstack.size() << std::endl << std::endl;
            for (auto elem : valu) {
                std::cout << elem.first << "=" << elem.second << std::endl;
            }
            std::cout << std::endl;

        } else if (inputstring != "quit") { // if this block is reached, no proper command has been given
            std::cout << "No proper command detected, try again." << std::endl << std::endl;
        }
    }

    return result;
}