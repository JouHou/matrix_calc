/**
    \file compositeelement.hpp
    \brief Header for CompositeElement class
*/
#ifndef COMPOSITEELEMENT_H_INCLUDED
#define COMPOSITEELEMENT_H_INCLUDED
#include <string>
#include <memory>
#include <functional>
#include "valuation.hpp"
#include "element.hpp"

/**
    \class CompositeElement
    \brief A class for composite elements
*/      
class CompositeElement : public Element {
    private:
        std::unique_ptr<Element> oprnd1; // Lefthand operand
        std::unique_ptr<Element> oprnd2; // Righthand operand
        std::function<int(int,int)> op_fun; // Mathematical operation
        char op_ch; // For printing purposes
    public:

        /**
            \brief Construct a new Composite Element object from parameters
            \param e1 Element&
            \param e2 Element&
            \param op std::function<int(int,int)>
            \param opc char
         */
        CompositeElement(const Element& e1, const Element& e2, const std::function<int(int,int)>& op, char opc);

        /**
            \brief Construct a new Composite Element object by copying        
            \param e CompositeElement& to be copied from
         */
        CompositeElement(const CompositeElement& e);

        /**
            \brief Copy assignment operator =         
            \param e CompositeElement&
            \return CompositeElement& 
         */
        CompositeElement& operator=(const CompositeElement& e);
        
        /**
            \brief Destroy the Composite Element object         
         */
        virtual ~CompositeElement(){};

        /**
            \brief Cloning function for Composite Elements
            \return CompositeElement*
         */
        virtual CompositeElement* clone() const override;

        /**
            \brief Function to transform CompositeElement to string
            \return std::string
         */
        std::string toString() const override;
        
        /**
            \brief Evaluate function for CompositeElements
            \param v Valuation&    
            \return int     
         */
        int evaluate(const Valuation& v) const override;
};

#endif
