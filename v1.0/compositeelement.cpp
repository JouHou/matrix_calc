/**
   \file compositeelement.cpp
   \brief Source code for CompositeElement class
*/

#include <string>
#include <sstream>
#include <memory>
#include "compositeelement.hpp"

CompositeElement::CompositeElement(const Element& e1, const Element& e2, const std::function<int(int,int)>& op, char opc) 
: oprnd1{std::unique_ptr<Element>(e1.clone())}, oprnd2{std::unique_ptr<Element>(e2.clone())}, op_fun{op}, op_ch{opc} {}

CompositeElement::CompositeElement(const CompositeElement& e)
: oprnd1{std::unique_ptr<Element>(e.oprnd1->clone())}, oprnd2{std::unique_ptr<Element>(e.oprnd2->clone())}, op_fun{e.op_fun}, op_ch{e.op_ch} {}

CompositeElement& CompositeElement::operator=(const CompositeElement& e) {
   if (this == &e) return *this;

   oprnd1 = std::unique_ptr<Element>(e.oprnd1->clone());
   oprnd2 = std::unique_ptr<Element>(e.oprnd2->clone());
   op_fun = e.op_fun;
   op_ch = e.op_ch;
   return *this;
}

CompositeElement* CompositeElement::clone() const {
   return new CompositeElement(*this);
}

std::string CompositeElement::toString() const {
   std::stringstream strm;
   strm << "(" << oprnd1->toString() << op_ch << oprnd2->toString() << ")";
   return strm.str(); 
}

int CompositeElement::evaluate(const Valuation& v) const {
   return op_fun(oprnd1->evaluate(v),oprnd2->evaluate(v));
}
